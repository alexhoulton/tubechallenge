<?php
header('Content-Type: text/css');
session_start();
//Autoload dependencies
require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
//Database
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/db_config.php');
//Objects
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/objects.class.php');
//API
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/mode.class.php');
//ModeInit
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/modeInit.php');

foreach ($mode->getLines() as $line) {
?>
.ui.<?=$line['line_name']?>.label, .ui.<?=$line['line_name']?>.labels .label, .ui.<?=$line['line_name']?>.progress>.bar {
    background-color: #<?=$line['line_colour']?>!important;
    border-color: #<?=$line['line_colour']?>!important;
    color: #fff!important;
}

.ui.<?=$line['line_name']?>.ribbon.label {
    border-color: #<?=$line['line_colour']?>!important;
}

<?php
}
?>
