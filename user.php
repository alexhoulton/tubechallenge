<?php
//Header
require_once('includes/header.php');

if(!isset($_GET['id']) || $_GET['id'] == '') {
  echoError('Invalid User', 'The user specified was not found. Please try again.');
  require_once('includes/footer.php');
  die();
}

$user_id = $_GET['id'];

//get user data
$db->where('user_id', $user_id);
$user = $db->getOne('users');

if(!$user) {
  $db->where('swarmUsername', $user_id);
  $user = $db->getOne('users');
}

if(!$user) {
  echoError('Invalid User', 'The user specified was not found. Please try again.');
  require_once('includes/footer.php');
  die();
}

//make sure that teh user id is an integer from here on in.
$user_id = $user['user_id'];

if(isset($_SESSION['user']['user_id']) && ($_SESSION['user']['user_id'] == $user_id)) {
  $guest = false;
} else {
  $guest = true;
}

$totalVisited = $mode->getTotalVisitedStations($user_id);
$totalStations = $mode->getTotalStations();
$progress = $totalVisited / $totalStations;
$progress_percent = floor(($progress) * 100);
?>
<?php
if(!$guest) { ?>
<div class="ui inline profile nag">
  <span class="title">
    This is your public profile page, you can share it with friends so they can view your progress!
  </span>
  <i class="close icon"></i>
</div>
<?php } ?>

<div class="ui one column stackable grid container clear-margin-top">
  <div class="column">
    <h1 class="ui dividing header"><?=$user['first_name']?>'s Tube Challange</h1>
  </div>
</div>

<div class="ui three column stackable grid container">
  <div class="row">
    <div class="five wide center aligned column">
      <?=renderUserProfileCard($user, false)?>
      <?=renderProgressPercentageCard($progress_percent)?>
      <?=renderTotalStationsVisitedCard($totalVisited)?>
      <?=renderStationsToGoCard($totalStations - $totalVisited)?>
      <?=renderRecentlyCompletedUICard($user_id)?>
    </div>
    <div class="eleven wide column">
      <div id="map" class="bottom padding"></div>
      <?=renderMapDropdown()?>
      <div class="row linesProgress">
        <h3 class="ui dividing header padding top">Lines</h3>
        <table class="ui very basic compact unstackable single line table">
        <?php
        $lines = $mode->getLines(true, $user_id);

        foreach($lines as $line) { ?>
          <tr>
            <td class="three wide">
              <span class="displayName"><?=$line['display_name']?></span>
            </td>
            <td>
              <div class="ui <?=$line['line_name']?> progress lineProgress" data-value="<?=$line['visited']?>" data-total="<?=$line['station_count']?>">
                <div class="bar">
                  <div class="progress"></div>
                </div>
              </div>
            </td>
          </tr>
        <?php } ?>
      </table>
      </div>
    </div>

  </div>

</div>
<?=rendergMapJavaScript($user_id)?>
<script>
$( document ).ready(function() {
  $('#stations_progress').progress({
    percent: <?=$progress_percent?>
  });

  $('.lineProgress')
  .progress({
    label: 'ratio',
    text: {
      ratio: '{value} of {total}'
    }
  });

  function checkProgressBars() {
    $('.ui.progress.lineProgress').each(function() {
      if(($(this).children('.bar').children('.progress').width() + 10) > $(this).children('.bar').width()) {
        $(this).children('.bar').children('.progress').hide();
      } else {
        $(this).children('.bar').children('.progress').show();
      }
    });
  }

  $( window ).resize($.debounce( 250, function() {
    setTimeout(function(){
      checkProgressBars();
    }, 500);
  }));

  setTimeout(function(){
    checkProgressBars();
  }, 500);

  <?php
  if(!$guest) { ?>
  // Automatically shows on init if cookie isnt set
  $('.profile.nag')
    .nag({
      key      : 'profile-explanation',
      value    : true
    });
  <?php } ?>
});
</script>
<?php
//Footer
require_once('includes/footer.php');
?>
