<?php
//Header
require_once('includes/header.php');
//Authentication
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/authentication.php');

if($auth->checkPermissions(100)) {
  if($modeController->current_mode==1) {
    $stations = $mode->populate_database($mode->getLines(), true);
  }

  if($modeController->current_mode==2) {
    $mode->populate_database(true);
  }
}

?>

<?php
//Footer
require_once('includes/footer.php');
 ?>
