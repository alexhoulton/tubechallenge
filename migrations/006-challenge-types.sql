CREATE TABLE `tube`.`modes` ( `mode_id` INT(11) NOT NULL AUTO_INCREMENT , `mode_name` VARCHAR(255) NOT NULL , `enabled` TINYINT NOT NULL DEFAULT '1' , PRIMARY KEY (`mode_id`)) ENGINE = InnoDB;

INSERT INTO `modes` (`mode_id`, `mode_name`, `enabled`) VALUES ('1', 'Tube Map', '1'), ('2', 'National Rail', '1');

ALTER TABLE `users` ADD `current_mode` INT NULL DEFAULT NULL AFTER `date_pref`;

ALTER TABLE `users` ADD FOREIGN KEY (`current_mode`) REFERENCES `modes`(`mode_id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `modes` ADD `mode_glyph` VARCHAR(255) NOT NULL AFTER `enabled`;

UPDATE `modes` SET `mode_glyph` = 'subway' WHERE `modes`.`mode_id` = 1;

UPDATE `modes` SET `mode_glyph` = 'train' WHERE `modes`.`mode_id` = 2;

ALTER TABLE `stations` ADD `mode_id` INT(11) NOT NULL DEFAULT '1' AFTER `station_id`;

ALTER TABLE `stations` ADD FOREIGN KEY (`mode_id`) REFERENCES `modes`(`mode_id`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `modes` ADD `class` VARCHAR(255) NOT NULL AFTER `mode_glyph`;

UPDATE `modes` SET `class` = 'nr' WHERE `modes`.`mode_id` = 2;

UPDATE `modes` SET `class` = 'tfl' WHERE `modes`.`mode_id` = 1;

ALTER TABLE `tfl_lines` ADD `mode_id` INT(11) NOT NULL AFTER `line_id`;

UPDATE `tfl_lines` SET `mode_id` = 1;

ALTER TABLE `tfl_lines` ADD FOREIGN KEY (`mode_id`) REFERENCES `modes`(`mode_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
