ALTER TABLE `user_checkins` ADD `type` INT(1) NOT NULL AFTER `checkin_id`;

UPDATE `user_checkins` SET type = 1 WHERE visitedAt = 0;

UPDATE `user_checkins` SET type = 2 WHERE visitedAt != 0;
