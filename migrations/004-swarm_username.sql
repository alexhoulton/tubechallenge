ALTER TABLE `users` DROP `swarm_username`;
ALTER TABLE `users` ADD `swarmUsername` VARCHAR(500) NOT NULL AFTER `canonicalUrl`;
