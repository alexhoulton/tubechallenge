<?php
//Header
require_once('includes/header.php');
//Authentication
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/authentication.php');

if(!$auth->checkPermissions(10)) {
  echo 'Access denied';
  die();
}
?>
<div class="ui one column stackable grid container clear-margin-top">
  <div class="column">
    <h1 class="ui dividing header">Leaderboard</h1>
  </div>
</div>

<div class="ui two column stackable grid container">
  <div class="column">
    <table id="leaderboardTable" class="ui selectable celled sortable unstackable table">
  <thead>
    <tr><th>Name</th>
    <th>Stations Completed</th>
    <th>%</th>
  </tr></thead>
  <tbody>
    <?php
    //show lines as tabs
    $users = getLeaderboardData();

    foreach($users as $user) { ?>

    <tr>
      <td>
        <h4 class="ui image header">
          <img src="<?=$user['image']?>" class="ui mini rounded image">
          <div class="content">
            <?=$user['first_name'] . " " . $user['last_name'][0]?>
            <div class="sub header"><a href="/user/<?=$user['swarmUsername']?>"><?=$user['swarmUsername']?></a>
          </div>
        </div>
      </h4></td>
      <td>
        <?=$user['visited']?>
      </td>
      <td><?=$user['progress']?>%</td>
    </tr>

  <?php } ?>
  </tbody>
</table>
</div>

<script>
$( document ).ready(function() {

    $('#leaderboardTable').DataTable({
        "info":     false,
        "bLengthChange": false,
        "bPaginate": false,
        "bFilter": false,
        "bAutoWidth": true,
        "autoWidth": true,
        "aaSorting": [[2,'desc']],
    });

});
</script>

<?php
//Footer
require_once('includes/footer.php');
 ?>
