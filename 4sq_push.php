<?php
//Autoload dependencies
require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
//Database
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/db_config.php');
//Objects
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/objects.class.php');
//4sq
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/foursquare.php');

if(!isset($_POST['checkin']) || $_POST['checkin'] == '') {
  http_response_code(500);
  die();
}

$checkin = json_decode($_POST['checkin'], true);

//get user
$user_id_4sq = $checkin['user']['id'];

//get place
$place_4sq_id = $checkin['venue']['id'];
$place_name = $checkin['venue']['name'];

//get timestamp
$time = $checkin['createdAt']; //epoch
$timezone_offset = $checkin['timeZoneOffset']; //this is in minutes

//check if the place exists in the DB already

$db->where('4sq_id', $place_4sq_id);
$place_id = $db->getValue('4sq_places', 'id');

if(!$place_id) {
  //we do not have the place, do the insertings
  $data = Array('4sq_id' => $place_4sq_id,
                '4sq_name' => $place_name);

  $place_id = $db->insert('4sq_places', $data);

}

//get the user id

$db->where('4sq_user_id', $user_id_4sq);
$user_id = $db->getValue('users', 'user_id');

if(!$user_id) {
  http_response_code(501);
  die();
}

//insert the checkin

$data = Array('user_id' => $user_id,
              '4sq_place_id' => $place_id,
              'visitedAt' => $time,
              'timeZoneOffset' => $timezone_offset
            );

$checkin_id = $db->insert('user_checkins', $data);

if(!$checkin_id) {
  error_log($db->getLastQuery());
  http_response_code(502);
  die();
}
