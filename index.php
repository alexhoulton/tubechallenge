<?php
//Header
require_once('includes/header.php');
//Authentication
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/authentication.php');


?>
<div class="ui two column stackable grid container clear-margin-top">
  <div class="column">
    <h1 class="ui header"><?=$_SESSION['user']['first_name']?>'s <?=$modeController->getCurrentMode()['mode_name']?> Challange</h1>
  </div>
  <?php
  $years = getUserYearsWithCheckins();

  $activeYear = date('Y', $_SESSION['user']['date_pref']);

  if(count($years)) {
    $db->where('user_id', $_SESSION['user']['user_id']);
    $db->where('type', 2);
    $job = $db->getOne('jobs');

  ?>
  <div id="timeSelector" class="right aligned column" <?=$job['progress'] < 100 ? 'style="display:none"' : ''?>>
    <div class="ui buttons">
      <div class="ui <?=$_SESSION['user']['date_pref']<=0 ? 'active ' : ''?>button year_pref" data-pref="1970">All Time</div>
      <?php
      if(count($years) >= 1) { ?>
      <div class="ui <?=$years[0]['year']==$activeYear ? 'active ' : ''?>button year_pref" data-pref="<?=$years[0]['year']?>"><?=$years[0]['year']?></div>
      <?php
      }
      if(count($years) >= 2) {  ?>
        <div class="ui floating dropdown icon button">
          <i class="dropdown icon"></i>
          <div class="menu">
            <?php
            for($x=1;$x<count($years);$x++) {
            ?>
            <div class="item <?=$years[$x]['year']==$activeYear ? 'active ' : ''?>year_pref" data-pref="<?=$years[$x]['year']?>"><?=$years[$x]['year']?></div>
          <?php } ?>
        </div>
      </div>
      <?php
      }
      ?>


    </div>
  </div>
  <?php } ?>
</div>

<?=renderHomepage()?>

<script>
$( document ).ready(function() {
  $('.year_pref').on('click', function() {
    if($(this).hasClass('active')) { } else {

      $('.year_pref.active').removeClass('active');
      $(this).addClass('active');

      $.post('/includes/updateDatePref', {date_pref: $(this).data('pref')}, function() {
        location.reload();
      });
    }
  });
});
</script>

<?php
//Footer
require_once('includes/footer.php');
 ?>
