<?php
//Header
require_once('includes/header.php');
//Authentication
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/authentication.php');

?>
<div class="ui one column stackable grid container clear-margin-top">
  <div class="column">
    <h1 class="ui dividing header">Stations by Zone</h1>
  </div>
</div>

<div class="ui two column stackable grid container">
  <div class="column">
    <table id="zone_table" class="ui selectable celled sortable unstackable table">
      <thead>
        <tr>
          <th>Zone</th>
          <th>Stations</th>
          <th>Complete</th>
          <th>%</th>
        </tr>
      </thead>
      <tbody>
        <?php
        //show zones
        $zones = $mode->getZones(true, $_SESSION['user']['user_id']);
        $count = 0;

        foreach($zones as $zone) { ?>

          <tr data-zone="<?=$zone['zone_id']?>">
            <td class="collapsing">
              <div class="ui ribbon label"><?=$zone['display_name']?></div>
            </td>
            <td><?=$zone['station_count']?></td>
            <td><?=$zone['visited']?></td>
            <td><?=floor($zone['visited'] / $zone['station_count'] * 100)?>%</td>
          </tr>

        <?php $count++; } ?>

      </tbody>
    </table>
  </div>
  <div id="zone_col" class="column">
    <div class="ui message">
      <div class="header">
        Select a zone to see stations...
      </div>
    </div>
  </div>
</div>

<script>
$( document ).ready(function() {
    $('#zone_table').DataTable({
        "info":     false,
        "bLengthChange": false,
        "bPaginate": false,
        "bFilter": false,
        "columnDefs": [
          { "type": "num", "Targets": [ 1,2 ] },
        ]
    });

    $('#zone_table tbody tr').on('click', function() {
      $.get('/includes/stations_by_zone?zone_id=' + $(this).data('zone'), function(data) {
        $('#zone_col').html(data);

        // Scroll to a certain element
        if( screen.width <= 768 ) {
          document.querySelector('#zone_header').scrollIntoView({
            behavior: 'smooth'
          });
        }

        //init the DataTable
        $('#zone_col table').DataTable({
            "info":     false,
            "pageLength": 12,
            "bLengthChange": false,
            "bFilter": false,
            "aoColumnDefs": [
              { "iDataSort": 3, "aTargets": [ 1 ] },
              { "aTargets": [ 2, 3 ], "visible": false },
            ]
        });

      });
    });

    $(document).on('click', '.unmapped-station', function(ev) {
      ev.preventDefault();
      var element = $(this);

      $('.ui.modal').modal('show').modal({
        onApprove: function () {

          $.post( "/includes/4sq_save.php", { station_id: $('#station_id').val(),
                                              place_id  : $('.ui.dropdown').dropdown('get value')[1],
                                              place_name: $('.ui.dropdown').dropdown('get text')[1]
                                            } );

        }});

      $('.ui.dropdown')
          .dropdown({
            apiSettings: {
              // this url parses query server side and returns filtered results
              url: '/includes/4sq_search?q={query}',
              saveRemoteData: false
            },
          });

          $('#station_name').text(element.text());
          $('#station_id').val(element.data('station'));
          $('#station_search').val(element.text());
    });


});
</script>
<div class="ui modal">
  <i class="close icon"></i>
  <div class="header">
    Map Foursquare place for <span id="station_name"></span>
  </div>
  <div class="content">
    <div class="ui grid">
      <div class="three wide column">
        <div class="ui small image">
          <img src="/dist/images/swarm.png">
        </div>
      </div>
      <div class="thirteen wide column">
        <div class="ui header">Please enter the station name..</div>
        <div class="ui form">
          <div class="two fields">
            <div class="field">
                <label>Foursquare Place</label>
                <input type="hidden" id="station_id" name="station_id" value="0"></input>
                <div class="ui search selection dropdown">
                  <input type="hidden">
                  <i class="dropdown icon"></i>
                  <input id="station_search" type="text" class="search" tabindex="0"><span class="sizer"></span>
                  <div class="default text"></div>
                <div class="menu transition hidden" tabindex="-1"></div></div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
  <div class="actions">
    <div class="ui black deny button">
      Go Back
    </div>
    <div class="ui positive right labeled icon button">
      Map Station
      <i class="checkmark icon"></i>
    </div>
  </div>
</div>

<?php
//Footer
require_once('includes/footer.php');
 ?>
