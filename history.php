<?php
//Header
require_once('includes/header.php');
//Authentication
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/authentication.php');

$progress = $mode->getTotalVisitedStations() / $mode->getTotalStations();
$progress_percent = floor(($progress) * 100);

// $foursquare->SetAccessToken($_SESSION['user']['4sq_token']);
//
// $start_time = time();
// $db->startTransaction();
// getCheckins($start_time);
// $db->commit();
//
// die();

?>
<div class="ui one column stackable grid container clear-margin-top">
  <div class="column">
    <h1 class="ui dividing header">History</h1>
  </div>
</div>

<div class="ui one column unstackable grid container">
  <div class="row">
    <div class="ui pointing secondary menu">
      <a class="<?=$r == "/history/visits" ? "active " : ''?>item" data-tab="first">Recently Visited</a>
      <a class="<?=$r == "/history/completed" ? "active " : ''?>item" data-tab="second">Recently Completed</a>
      <a class="<?=$r == "/history/top" ? "active " : ''?>item" data-tab="third">Top Stations</a>
    </div>
    <div class="ui bottom attached <?=$r == "/history/visits" ? "active " : ''?>tab segment" data-tab="first">
      <table id="recentlyVisitedTable" class="ui celled table showTopRow">
        <thead>
          <tr>
            <th>Station</th>
            <th>Date Visited</th>
            <!-- <th>Line</th> -->
          </tr>
        </thead>
        <tbody>

      <?php
      $stations = $mode->getRecentlyVisitedStations(-1);

      foreach($stations as $station) {
        ?>
        <tr>
          <td>
            <a class="stationLink" href="#" data-station="<?=$station['station_id']?>">
              <?=$mode->stripStationName($station['commonName'])?>
            </a>
          </td>
          <td><?=date('d/m/y H:i', $station['visitedAt'])?></td>
          <!-- <td></td> -->
        </tr>
        <?php
      }
       ?>
       </tbody>
     </table>
    </div>
    <div class="ui bottom attached <?=$r == "/history/completed" ? "active " : ''?>tab segment" data-tab="second">
      <table id="recentlyCompletedTable" class="ui celled table showTopRow">
        <thead>
          <tr>
            <th>Station</th>
            <th>Date Completed</th>
            <!-- <th>Line</th> -->
          </tr>
        </thead>
        <tbody>
          <?php
          $stations = $mode->getRecentlyCompletedStations(null, -1);

          foreach($stations as $station) {
            ?>
            <tr>
              <td>
                <a class="stationLink" href="#" data-station="<?=$station['station_id']?>">
                  <?=$mode->stripStationName($station['commonName'])?>
                </a>
              </td>
              <td><?=date('d/m/y H:i', $station['visitedAt'])?></td>
              <!-- <td></td> -->
            </tr>
            <?php
          }
           ?>
           </tbody>
          </table>
    </div>
    <div class="ui bottom attached <?=$r == "/history/top" ? "active " : ''?>tab segment" data-tab="third">
      <table id="topStationsTable" class="ui celled table showTopRow">
        <thead>
          <tr>
            <th>Station</th>
            <th>Visits</th>
            <th>Last Visited</th>
          </tr>
        </thead>
        <tbody>

      <?php
      $stations = $mode->getTopStations(-1);

      foreach($stations as $station) {
        ?>
        <tr>
          <td><a class="stationLink" href="#" data-station="<?=$station['station_id']?>">
            <?=$mode->stripStationName($station['commonName'])?>
          </a></td>
          <td>
            <?=$station['checkins']?>
          </td>
          <td>
            <?=isset($station['lastCheckin']) && $station['lastCheckin'] ? date('d/m/y H:i', $station['lastCheckin']) : ''?>
          </td>
        </tr>
        <?php
      }
       ?>
       </tbody>
     </table>
    </div>
  </div>
</div>

<script>
$( document ).ready(function() {
  $('.menu .item').tab({
  });

  $.fn.dataTable.moment( 'DD/MM/YY HH:mm' ); //php equiv d/m/y H:i

  $('table:not(#topStationsTable)').DataTable({
      "bFilter": true,
      "order": [[ 1, "desc" ]],
      "columnDefs": [
        { "type": "date-uk", "Targets": [ 1 ] },
      ]
  });

  $('#topStationsTable').DataTable({
      "bFilter": true,
      "order": [[ 1, "desc" ]],
      "columnDefs": [
        { "type": "date-uk", "Targets": [ 3 ] },
      ]
  });
});
</script>

<?php
//Footer
require_once('includes/footer.php');
 ?>
