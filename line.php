<?php
//Header
require_once('includes/header.php');
//Authentication
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/authentication.php');
?>
<div class="ui one column stackable grid container clear-margin-top">
  <div class="column">
    <h1 class="ui dividing header">Stations by Line</h1>
  </div>
</div>

<div class="ui two column stackable grid container">
  <div class="column">
    <table id="lines_table" class="ui selectable celled sortable unstackable table">
      <thead>
        <tr>
          <th>Line</th>
          <th>Stations</th>
          <th>%</th>
        </tr>
      </thead>
      <tbody>
        <?php
        //show lines as tabs
        $lines = $mode->getLines(true, $_SESSION['user']['user_id']);
        $count = 0;

        foreach($lines as $line) { ?>

          <tr data-line="<?=$line['line_id']?>">
            <td class="collapsing">
              <div class="ui <?=$line['line_name']?> ribbon label"><?=$line['display_name']?></div>
            </td>
            <td><?=$line['visited']?>/<?=$line['station_count']?></td>
            <td><?=floor($line['visited'] / $line['station_count'] * 100)?>%</td>
          </tr>

        <?php $count++; } ?>

      </tbody>
    </table>
  </div>
  <div id="line_col" class="column">
    <div class="ui message">
      <div class="header">
        Select a line to see stations...
      </div>
    </div>
  </div>
</div>

<script>
$( document ).ready(function() {

    $('#lines_table').DataTable({
        "info":     false,
        "bLengthChange": false,
        "bPaginate": false,
        "bFilter": false,
        "bAutoWidth": true,
        "autoWidth": true,
        "columnDefs": [
          { "type": "num", "Targets": [ 1,2 ] },
        ]
    });

    $('#lines_table tbody tr').on('click', function() {
      $.get('/includes/stations_by_line.php?line_id=' + $(this).data('line'), function(data) {
        $('#line_col').html(data);

        // Scroll to a certain element
        if( screen.width <= 768 ) {
          document.querySelector('#lineHeader').scrollIntoView({
            behavior: 'smooth'
          });
        }

        //init the DataTable
        $('#line_table').DataTable({
            "info":     false,
            "pageLength": 12,
            "bLengthChange": false,
            "bFilter": false,
            "bAutoWidth": true,
            "autoWidth": true,
            "columnDefs": [
              { "iDataSort": 3, "targets": [ 1 ] },
              { "targets": [ 2, 3 ], "visible": false },
            ]
        });

      });
    });

    $(document).on('click', '.unmapped-station', function(ev) {
      ev.preventDefault();
      var element = $(this);

      $('.ui.foursquare.modal').modal('show').modal({
        onApprove: function () {

          $.post( "/includes/4sq_save.php", { station_id: $('#station_id').val(),
                                              place_id  : $('.ui.dropdown.foursquare').dropdown('get value'),
                                              place_name: $('.ui.dropdown.foursquare').dropdown('get text')
                                            } );

        }});

      $('.ui.dropdown.foursquare')
          .dropdown({
            apiSettings: {
              // this url parses query server side and returns filtered results
              url: '/includes/4sq_search.php?q={query}',
              saveRemoteData: false
            },
          });

          $('#station_name').text(element.text());
          $('#station_id').val(element.data('station'));
          $('#station_search').val(element.text());
    });


});
</script>
<div class="ui foursquare modal">
  <i class="close icon"></i>
  <div class="header">
    Map Foursquare place for <span id="station_name"></span>
  </div>
  <div class="content">
    <div class="ui grid">
      <div class="three wide column">
        <div class="ui small image">
          <img src="/dist/images/swarm.png">
        </div>
      </div>
      <div class="thirteen wide column">
        <div class="ui header">Please enter the station name..</div>
        <div class="ui form">
          <div class="two fields">
            <div class="field">
                <label>Foursquare Place</label>
                <input type="hidden" id="station_id" name="station_id" value="0"></input>
                <div class="ui search selection dropdown foursquare">
                  <input type="hidden">
                  <i class="dropdown icon"></i>
                  <input id="station_search" type="text" class="search" tabindex="0"><span class="sizer"></span>
                  <div class="default text"></div>
                <div class="menu transition hidden" tabindex="-1"></div></div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
  <div class="actions">
    <div class="ui black deny button">
      Go Back
    </div>
    <div class="ui positive right labeled icon button">
      Map Station
      <i class="checkmark icon"></i>
    </div>
  </div>
</div>

<?php
//Footer
require_once('includes/footer.php');
 ?>
