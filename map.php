<?php
//Header
require_once('includes/header.php');
//Authentication
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/authentication.php');

?>
<div class="ui one column stackable grid container clear-margin-top">
  <div class="column">
    <h1 class="ui dividing header">Map</h1>
  </div>
</div>
<div class="ui one column grid container">
  <div class="column">
    <?=renderMapDropdown()?>
  </div>
</div>

<div id="map" class="top padding"></div>
<?=rendergMapJavaScript($_SESSION['user']['user_id'])?>
<?php
//Footer
require_once('includes/footer.php');
 ?>
