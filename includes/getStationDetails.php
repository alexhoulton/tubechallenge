<?php
ini_set('session.gc_maxlifetime', 604800);
ini_set('session.cookie_lifetime', 604800);
session_start();
//Autoload dependencies
require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
//Database
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/db_config.php');
//Objects
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/objects.class.php');
//Classes
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/mode.class.php');
//4sq
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/foursquare.php');
//Functions
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/functions.php');
//ModeInit
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/modeInit.php');

if(!isset($_GET['station_id']) || $_GET['station_id'] == '') {
  die('Invalid station ID');
}

$station_id = $_GET['station_id'];

echo renderStationDetails($station_id);
die();
