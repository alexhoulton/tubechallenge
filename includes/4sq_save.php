<?php
session_start();
//Autoload dependencies
require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
//Database
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/db_config.php');
//4sq
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/foursquare.php');

if(isset($_POST['station_id']) && $_POST['station_id'] != '' && isset($_POST['place_id']) && $_POST['place_id'] != '' && isset($_POST['place_name']) && $_POST['place_name'] != '' ) {

  $station_id = $_POST['station_id'];
  $place_id = $_POST['place_id'];
  $place_name = $_POST['place_name'];

  //check if the 4sq place already exists

  $db->where('4sq_id', $place_id);
  $foursquare_places_id = $db->getValue('4sq_places', 'id');

  if(!$foursquare_places_id) {
    //create it

    $data = Array('4sq_id' => $place_id,
                  '4sq_name' => $place_name);

    $foursquare_places_id = $db->insert('4sq_places', $data);

  }

  //now insert the mapping

  $data = Array('station_id' => $station_id,
                '4sq_place_id' => $foursquare_places_id);

  $db->insert('station_4sq_id', $data);
}
