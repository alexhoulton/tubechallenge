<?php
session_start();
//Autoload dependencies
require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
//Database
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/db_config.php');
//4sq
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/foursquare.php');
//Functions
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/functions.php');

// If the link has been clicked, and we have a supplied code, use it to request a token
if(array_key_exists("code",$_GET)){
	$code = $foursquare->GetToken($_GET['code'],$redirect_uri);
}

// If we have not received a token, redirect with error
if(!isset($code) || $code === 0){
	header("Location:/login?error=token_fail");
	die();
// Otherwise deal with the token
} else {
	//check if user already exists
	$db->where('4sq_token', $code);
	$user = $db->getOne('users');

	if(isset($user['user_id']) && $user['user_id'] > 0) {
		//we're good, log in
		$_SESSION['user'] = $user;
		if(array_key_exists("page",$_GET)){
			header("Location:" . $_GET['page']);
		} else {
			header("Location:/");
		}
		die();
	} else {
		//get user data
		$request = $foursquare->GetPrivate("users/self");
		$details = json_decode($request);
		$u = $details->response->user;

		//create the user account with the data we don't have

		$data = Array('4sq_token' 		=> $code,
									'4sq_user_id' 	=> $u->id,
									'first_name' 		=> $u->firstName,
									'last_name' 		=> $u->lastName,
									'email'					=> $u->contact->email,
									'canonicalUrl' 	=> $u->canonicalUrl,
									'swarmUsername'	=> str_replace('https://foursquare.com/', '', $u->canonicalUrl),
									'friendsCount' 	=> $u->friends->count,
									'joined4sq' 		=> $u->createdAt,
									'bio'						=> $u->bio);

		if(property_exists($u->contact,"twitter")){
				$data['twitter'] = $u->contact->twitter;
		}

		if(property_exists($u->contact,"facebook")){
				$data['facebook'] = $u->contact->facebook;
		}

		if(property_exists($u->photo,"prefix")){
				$data['image'] = $u->photo->prefix . '130x130' . $u->photo->suffix;
		}

		$user_id = $db->insert('users', $data);

		if($user_id) {
			$_SESSION['user'] = $data;
			$_SESSION['user']['user_id'] = $user_id;

			//setup a job and execute it

			$data = Array('type' => 1,
										'user_id' => $user_id);

			$initial_job_id = $db->insert('jobs', $data);

			$data = Array('type' => 2,
										'user_id' => $user_id);

			$checkin_job_id = $db->insert('jobs', $data);

			exec('bash -c "exec nohup setsid php /var/www/html/tubemap/includes/initial_pull.php ' . $initial_job_id . ' ' . $checkin_job_id .' > /dev/null 2>&1 &"');

		?>
		<!doctype html>
		<html>
		  <head>
		    <title>Tube Map Challenge</title>
		    <link rel="stylesheet" type="text/css" href="/dist/semantic/dist/semantic.min.css">
		    <link rel="stylesheet" type="text/css" href="/dist/css/custom.css">
		    <script src="/node_modules/jquery/dist/jquery.min.js"></script>
		    <script src="/dist/semantic/dist/semantic.min.js"></script>
		    <meta content="width=device-width, initial-scale=1.0, user-scalable=yes" name="viewport" />
		  </head>
		  <body>
			<div class="container ui two column centered grid">
				<div class="four column centered row">
			    <div class="column"></div>
			    <div class="column"></div>
			  </div>
			  <div class="column centered-text">
			    <a href="#" class="ui orange disabled loading button">
			      <img class="ui avatar image" src="/dist/images/swarm.png">
			      <span>Connect with Swarm</span>
			    </a>
			  </div>
				<div class="column centered-text row">
			    <h1>Hang tight, <?=$u->firstName?>.</h1>
			  </div>
				<div class="column centered-text row">
			    <h3>We're just logging you in now.. won't be long!</h3>
			  </div>
				<div class="column centered-text row">
					<div class="ui teal progress" id="progressBar" style="width:100%">
					  <div class="bar">
					    <div class="progress"></div>
					  </div>
					  <div id="progressLabel" class="label">Getting started...</div>
					</div>
				</div>
			  <div class="four column centered row">
			    <div class="column"></div>
			    <div class="column"></div>
			  </div>
			</div>
			<script>

			function checkJob() {
				$.get( "/includes/checkjob?job_id=<?=$initial_job_id?>", function( data ) {

					if(data == 40) {
						$('#progressLabel').text('Greasing the wheels...');
					}
					if(data == 75) {
						$('#progressLabel').text('Starting the engine...');
					}
					if(data == 100) {
						$('#progressLabel').text("Let's go!");
					}
					$('#progressBar').progress('set percent', data);
					if(data == 100) {
						window.location.replace("<?=array_key_exists("page",$_GET) ? $_GET['page'] : '/'?>");
					}
				});
			}
			$( document ).ready(function() {
				var t=setInterval(checkJob,1000);
			});
			</script>

			<?php
			die();
		}

	}

}

?>
