<div class="ui one column stackable grid container">
  <div class="row"></div>
  <div class="column">
    <h5 class="ui centered grey header">© <?=date("Y")?> <a target="_blank" href="https://www.alexhoulton.co.uk">Alex Houlton</a> | <a href="https://gitlab.com/alexhoulton/tubechallenge" target="_blank">This project is open source.</a></h5>
  </div>
</div>
