<?php
//Init the modeController class
$modeController = new mode($db);

$className = $modeController->getCurrentMode()['class'];
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/' . $className . '.api.class.php');
$mode = new $className($db);
?>
