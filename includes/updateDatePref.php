<?php
session_start();
//Autoload dependencies
require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
//Database
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/db_config.php');

if(isset($_POST['date_pref']) && $_POST['date_pref'] != '' && is_numeric($_POST['date_pref'])) {
  //update db
  $datePref = strtotime('January 1 ' .  $_POST['date_pref']);
  $data = Array('date_pref' => $datePref);

  $db->where('user_id', $_SESSION['user']['user_id']);
  $db->update('users', $data);

  $_SESSION['user']['date_pref'] = $datePref;
}
