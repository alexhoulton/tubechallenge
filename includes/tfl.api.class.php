<?php
//This file provides interaction with TFL

class tfl extends mode {
  var $api_root = "https://api.tfl.gov.uk/";

  private function send_api_call($endpoint) {
    //check cache first

    $url = $this->api_root . $endpoint;

    if($result = $this->mem->checkCache($url)) {
      return json_decode($result, true);
    } else {
      $result = file_get_contents($url);
      $this->mem->addCache($url, $result);
      return json_decode($result, true);
    }

  }

  public function stripStationName($station) {
    $station = str_replace(' Underground Station', '', $station);
    $station = str_replace(' Station', '', $station);

    return $station;
  }

  public function getStations($lines) {
    //takes $lines and returns an array of stations

    $return_stations = [];

    foreach($lines as $line) {
      $pulled_stations = $this->get_stations_by_line($line['line_name']);

      foreach($pulled_stations as $pulled_station) {
        $station = new station();

        $station->naptanID = $pulled_station['naptanId'];
        $station->commonName = $pulled_station['commonName'];
        $station->lat = $pulled_station['lat'];
        $station->lon = $pulled_station['lon'];

        $return_stations[$station->naptanID] = $station;
      }

    }

    return $return_stations;

  }

  private function get_stations_by_line($line, $api = true) {

    $base = "Line/";
    $end = "/stoppoints";

    $url = $base . $line . $end;

    if($api) {
      return $this->send_api_call($url);
    } else {
      //use db
    }
  }
  
  private function dealWithZones($station_id, $zone) {

    //make sure the zone we get from TFL is created properly

    $zone_id = $this->getZoneID($zone, $station_id);

    if(mb_strlen($zone) == 3) {
      //we gotta create two zone entries
      //try split based on the middle character being a separator
      $string = str_split($zone);

      $zone_1_id = $this->getZoneID($string[0]);
      $zone_2_id = $this->getZoneID($string[2]);

      $this->insertZoneEntry($station_id, $zone_1_id);
      $this->insertZoneEntry($station_id, $zone_2_id);

    } else {

      $this->insertZoneEntry($station_id, $zone_id);

    }

  }

  private function getZoneID($zone, $station_id = NULL) {

    if(!$zone) {
      if($station_id != NULL) {
        //check if special or tram or other
        $this->db->where('station_id', $station_id);
        $station_name = $this->db->getValue('stations', 'commonName');
        if(strpos($station_name, 'Tram') !== false) {
          $zone = 'T';
        } else {
          $zone = 'S';
        }
      }
    }

    //get zone_id
    $this->db->where('zone_name', $zone);
    $zone_id = $this->db->getValue('zones', 'zone_id');

    if(!$zone_id) {
      $data = Array('zone_name' => $zone, 'display_name' => $zone);

      $zone_id = $this->db->insert('zones', $data);
    }

    return $zone_id;
  }

  private function insertZoneEntry($station_id, $zone_id) {

    $data = Array('station_id' => $station_id, 'zone_id' => $zone_id);

    return $this->db->insert('stations_zones', $data);
  }

  public function populate_database($lines, $debug = false) {
    $this->db->startTransaction();
    foreach($lines as $line) {
      $pulled_stations = $this->get_stations_by_line($line['line_name'], true);

      foreach($pulled_stations as $pulled_station) {
        $id = NULL;

        $station = new station();

        $station->naptanID = $pulled_station['naptanId'];
        $station->commonName = $pulled_station['commonName'];
        $station->lat = $pulled_station['lat'];
        $station->lon = $pulled_station['lon'];
        $station->lastUpdated = time();

        foreach($pulled_station['additionalProperties'] as $additionalProperty) {
          if($additionalProperty['key'] == 'Zone') {
            $station->zone = $additionalProperty['value'];
            break;
          }
        }

        //this used to use onDuplicate but it just doesn't seem to return the correct row IDs.

        //check if the station exists
        $this->db->where('naptanId', $station->naptanID);
        $station_id = $this->db->getValue('stations', 'station_id');

        if($station_id) {
          $this->db->where('station_id', $station_id);
          $this->db->update ('stations', $station->getAsArray(1));
        } else {
          $station_id = $this->db->insert ('stations', $station->getAsArray(1));
          if($debug) {
            echo $this->db->getLastQuery() . '<br>';
          }
        }

        //deal with zones

        $this->dealWithZones($station_id, $station->zone);

        $data = Array('station_id' => $station_id,
                      'line_id' => $line['line_id'],
                      'lastUpdated' => time()
                     );

         //check if the line entry exists
         $this->db->where('station_id', $station_id);
         $this->db->where('line_id', $line['line_id']);
         $line_index = $this->db->getValue('lines_stations', 'id');

         if($line_index) {
           $this->db->where('id', $line_index);
           $this->db->update('lines_stations', $data);
         } else {
           $line_index = $this->db->insert('lines_stations', $data);
           if($debug) {
             echo $this->db->getLastQuery() . '<br>';
           }
         }

      }

    }
    $this->db->commit();

  }

}



 ?>
