<?php
//Objects
require_once('objects.class.php');
//Cache
require_once('cache.class.php');

class mode {
  var $mem;
  var $db;
  var $current_mode;

  public function __construct($db) {
    $this->db = $db;
    $this->mem = new cache();

    $this->current_mode = $_SESSION['user']['current_mode'] ?? 1;
  }

  public function getModes() {
    // returns all modes
    global $db;

    $db->where('enabled', 1);
    $modes = $db->get('modes');

    return $modes;
  }

  public function getCurrentMode() {
    // returns current mode
    global $db;

    $db->where('mode_id', $this->current_mode);
    $mode = $db->getOne('modes');

    return $mode;
  }

  public function getStationDetails($station_id) {
    global $foursquare;

    $this->db->where('stations.station_id', $station_id);
    $this->db->join('station_4sq_id', 'stations.station_id = station_4sq_id.station_id', 'LEFT');
    $this->db->join('4sq_places', 'station_4sq_id.4sq_place_id = 4sq_places.id', 'INNER');
    $station = $this->db->getOne('stations');

    //get the foursquare data

    $endpoint = "venues/" . $station['4sq_id'];
    $venue = $foursquare->GetPublic($endpoint);

    return json_encode($venue);
  }

  public function getLines($station_count = false, $user_id = false, $station_id = false) {

    $cols = Array('tfl_lines.*');

    $this->db->where('tfl_lines.mode_id', $this->current_mode);

    if($station_id) {
      $this->db->where('lines_stations.station_id', $station_id);
    }

    if($station_count || $station_id) {
      $this->db->join('lines_stations', 'tfl_lines.line_id = lines_stations.line_id', 'LEFT');
    }

    if($user_id) {

      $this->db->join('stations', 'lines_stations.station_id = stations.station_id', 'INNER');
      $this->db->join('station_4sq_id', 'stations.station_id = station_4sq_id.station_id', 'LEFT'); //might not be linked
      $this->db->join('4sq_places', 'station_4sq_id.4sq_place_id = 4sq_places.id', 'LEFT');
      $this->db->join('user_checkins', 'station_4sq_id.4sq_place_id = user_checkins.4sq_place_id AND user_checkins.user_id = ' . $user_id, 'LEFT'); //user might not have visited
      array_push($cols, 'count(distinct(user_checkins.4sq_place_id)) as visited');
    }

    if($station_count) {
      $this->db->groupBy('tfl_lines.line_id');
      array_push($cols, 'count(distinct(lines_stations.station_id)) as station_count');
    }
    $this->db->orderBy('display_name', 'ASC');
    return $this->db->get('tfl_lines', null, $cols);

  }

  public function getZonesByStation($station_id) {
    $cols = Array('zones.*');
    $this->db->where('stations.station_id', $station_id);
    $this->db->join('stations_zones', 'zones.zone_id = stations_zones.zone_id', 'INNER');
    $this->db->join('stations', 'stations_zones.station_id = stations.station_id', 'INNER');
    $zones = $this->db->get('zones', null, $cols);

    return $zones;
  }
  
  public function getZones($station_count = false, $user_id = false) {

    $cols = Array('zones.zone_id', 'display_name');

    $this->db->where('stations.mode_id', $this->current_mode);

    $this->db->join('stations_zones', 'stations.station_id = stations_zones.station_id', 'INNER');
    $this->db->join('zones', 'stations_zones.zone_id = zones.zone_id', 'INNER');

    if($station_count) {
      array_push($cols, 'count(distinct(stations.station_id)) as station_count');
    }

    if($user_id) {

      $this->db->join('station_4sq_id', 'stations.station_id = station_4sq_id.station_id', 'LEFT'); //might not be linked
      $this->db->join('4sq_places', 'station_4sq_id.4sq_place_id = 4sq_places.id', 'LEFT');
      $this->db->join('user_checkins', 'station_4sq_id.4sq_place_id = user_checkins.4sq_place_id AND user_checkins.user_id = ' . $_SESSION['user']['user_id'], 'LEFT'); //user might not have visited
      array_push($cols, 'count(distinct(user_checkins.4sq_place_id)) as visited');
    }

    $this->db->groupBy('zones.zone_id');
    $this->db->orderBy('zones.display_name', 'ASC');

    return $this->db->get('stations', null, $cols);

  }

  public function getTotalVisitedStations($user_id = NULL) {
    if(!$user_id) {
      $user_id = $_SESSION['user']['user_id'];
    }
    if(isset($_SESSION['user']['date_pref']) && $_SESSION['user']['date_pref'] > 0) {

      //get start of next year
      $year = strtotime('1 January ' . (date('Y', $_SESSION['user']['date_pref']) + 1));

      $this->db->where('user_checkins.visitedAt', $_SESSION['user']['date_pref'], '>=');
      $this->db->where('user_checkins.visitedAt', $year, '<');
    }
    $this->db->where('mode_id', $this->current_mode);
    $this->db->join('4sq_places', 'station_4sq_id.4sq_place_id = 4sq_places.id', 'INNER');
    $this->db->join('user_checkins', 'station_4sq_id.4sq_place_id = user_checkins.4sq_place_id AND user_checkins.user_id = ' . $user_id, 'INNER'); //user might not have visited
    $this->db->join('stations', 'station_4sq_id.station_id = stations.station_id', "INNER");

    return $this->db->getValue('station_4sq_id', 'count(distinct(station_4sq_id.station_id))');
  }

  public function getTotalStations() {
    $this->db->where('mode_id', $this->current_mode);
    $this->db->join('stations', 'station_4sq_id.station_id = stations.station_id', "INNER");

    return $this->db->getValue('station_4sq_id', 'count(distinct(station_4sq_id.station_id))');
  }

  public function stripStationName($station) {
    $station = str_replace(' Station', '', $station);

    return $station;
  }

  public function getRecentlyVisitedStations($number = 10) {
    // SELECT stations.station_id, commonName, visitedAt FROM `user_checkins`
    // JOIN 4sq_places on user_checkins.4sq_place_id = 4sq_places.id
    // JOIN station_4sq_id on user_checkins.4sq_place_id = station_4sq_id.4sq_place_id
    // JOIN stations on station_4sq_id.station_id = stations.station_id
    // WHERE user_id = 1
    // ORDER BY `visitedAt` DESC

    $cols = Array('stations.station_id', 'commonName', 'visitedAt', '4sq_id');

    $this->db->join('4sq_places', 'user_checkins.4sq_place_id = 4sq_places.id', 'INNER');
    $this->db->join('station_4sq_id', 'user_checkins.4sq_place_id = station_4sq_id.4sq_place_id', 'INNER');
    $this->db->join('stations', 'station_4sq_id.station_id = stations.station_id', 'INNER');
    $this->db->where('mode_id', $this->current_mode);
    $this->db->where('user_id', $_SESSION['user']['user_id']);
    $this->db->where('visitedAt', 0, '>'); //remove visit only entries
    $this->db->groupBy('checkin_id');
    $this->db->orderBy('visitedAt', 'DESC');

    if($number == -1) { $number = NULL; }

    return $this->db->get('user_checkins', $number, $cols);

  }

  public function getStationVisitData($station_id) {

    $cols = Array('stations.*', 'count(user_checkins.checkin_id) as checkins', '4sq_places.4sq_id',
                  'MAX(user_checkins.visitedAt) as lastCheckin',
                  'MIN(NULLIF(user_checkins.visitedAt, 0)) as firstCheckin');

    $this->db->where('stations.station_id', $station_id);
    $this->db->where('user_checkins.visitedAt', 0, '!=');
    $this->db->where('mode_id', $this->current_mode);

    $this->db->join('station_4sq_id', 'stations.station_id = station_4sq_id.station_id', 'LEFT');
    $this->db->join('4sq_places', 'station_4sq_id.4sq_place_id = 4sq_places.id', 'LEFT');
    $this->db->join('user_checkins', 'station_4sq_id.4sq_place_id = user_checkins.4sq_place_id AND user_checkins.user_id = ' . $_SESSION['user']['user_id'], 'INNER');
    $this->db->groupBy('stations.station_id');

    return $this->db->getOne('stations', $cols);

  }

  public function getTopStations($number = 5) {
    // SELECT stations.*, count(user_checkins.checkin_id) as checkins, 4sq_places.4sq_id FROM stations
    // LEFT JOIN station_4sq_id on stations.station_id = station_4sq_id.station_id
    // LEFT JOIN 4sq_places on station_4sq_id.4sq_place_id = 4sq_places.id
    // LEFT JOIN user_checkins on station_4sq_id.4sq_place_id = user_checkins.4sq_place_id AND user_checkins.user_id = 1
    // GROUP BY stations.station_id
    // ORDER BY `checkins`  DESC

    $cols = Array('stations.*', 'count(distinct(user_checkins.checkin_id)) as checkins', '4sq_places.4sq_id',
                  'MAX(user_checkins.visitedAt) as lastCheckin');

    $this->db->where('user_checkins.visitedAt', 0, '!=');
    $this->db->where('mode_id', $this->current_mode);

    $this->db->join('station_4sq_id', 'stations.station_id = station_4sq_id.station_id', 'LEFT');
    $this->db->join('4sq_places', 'station_4sq_id.4sq_place_id = 4sq_places.id', 'LEFT');
    $this->db->join('user_checkins', 'station_4sq_id.4sq_place_id = user_checkins.4sq_place_id AND user_checkins.user_id = ' . $_SESSION['user']['user_id'], 'INNER');
    $this->db->groupBy('4sq_id');
    $this->db->orderBy('checkins', 'DESC');

    if($number == -1) { $number = NULL; }

    return $this->db->get('stations', $number, $cols);

  }

  public function getRecentlyCompletedStations($user_id = NULL, $number = 10) {

    if(!$user_id) {
      $user_id = $_SESSION['user']['user_id'];
    }

    if($number == -1) { $number = ''; } else { $number = 'LIMIT ' . $number; }

    //this is horrible please somebody fix it :)
    return $this->db->rawQuery("
                          SELECT * FROM (
                            SELECT stations.station_id,
                                   commonName,
                                   min(NULLIF(visitedAt, 0)) as visitedAt,
                                   4sq_id
                            FROM user_checkins
                            INNER JOIN 4sq_places on user_checkins.4sq_place_id = 4sq_places.id
                            INNER JOIN station_4sq_id on user_checkins.4sq_place_id = station_4sq_id.4sq_place_id
                            INNER JOIN stations on station_4sq_id.station_id = stations.station_id
                            WHERE user_id = ?
                            AND visitedAt > '0'
                            AND mode_id = ?
                            GROUP BY stations.station_id
                            ORDER BY visitedAt DESC
                          ) as A
                          GROUP BY 4sq_id
                          ORDER BY visitedAt DESC
                          " . $number,
                          Array($user_id, $this->current_mode)
                        );

  }
}
 ?>
