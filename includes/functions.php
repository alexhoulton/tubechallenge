<?php
//misc functions

function get4sqPlaceID($venue_id, $venue_name) {
  global $db;

  $db->where('4sq_id', $venue_id);
  $place_id = $db->getValue('4sq_places', 'id');

  if(!$place_id) {
    //create the place
    $data = Array('4sq_id' => $venue_id,
                  '4sq_name' => $venue_name
                  );

    $place_id = $db->insert('4sq_places', $data);
  }

  return $place_id;
}

function getCheckins($start_time, $initial = false, $user_id = NULL, $job_id = NULL) {
  global $foursquare;
  global $db;
  global $totalCheckins;
  global $checkinsPulled;
  global $db2;

  if(!$user_id) {
    $user_id = $_SESSION['user']['user_id'];
  }

  $params = array("limit"=> 250,
                  "beforeTimestamp" => $start_time,
                  "sort" => "newestfirst");

  $request = $foursquare->GetPrivate("users/self/checkins", $params);
  $details = json_decode($request, true);

  $totalCheckins = $details['response']['checkins']['count'];

  if(!count($details['response']['checkins']['items'])) {
    return;
  }

  foreach($details['response']['checkins']['items'] as $checkin) {

    //make sure the place exists
    $place_id = get4sqPlaceID($checkin['venue']['id'], $checkin['venue']['name']);

    $data = Array('user_id'        => $user_id,
                  '4sq_place_id'   => $place_id,
                  '4sq_checkin_id' => $checkin['id'],
                  'visitedAt'      => $checkin['createdAt'],
                  'timeZoneOffset' => $checkin['timeZoneOffset'],
                  'type'           => 2
                  );

    $db->insert('user_checkins', $data);

    $last_timestamp = $checkin['createdAt'];
  }

  if($initial) {
    return;
  }

  $checkinsPulled+=250;

  //we're done with this batch, go get more
  if($job_id) {
    //update job
    $data = Array('progress' => floor(($checkinsPulled / $totalCheckins) * 100));

    $db2->where('job_id', $job_id);
    $db2->update('jobs', $data);

  }

  getCheckins($last_timestamp, false, $user_id, $job_id);
  return;
}

function updateProgress($job_id, $progress) {
  global $db;

  $data = Array('progress' => $progress);

  $db->where('job_id', $job_id);
  $db->update('jobs', $data);

  return true;
}

function renderRecentlyCompleted($user_id = NULL) {
  global $db;
  global $mode;
  global $guest;

  if(!$user_id) {
    $user_id = $_SESSION['user']['user_id'];
  }

  $output = '';
  $output .= '<div class="ui small feed">';

  $stations = $mode->getRecentlyCompletedStations($user_id);
  $checkin_day_start = PHP_INT_MAX;

  foreach($stations as $station) {

    $new_day_start = strtotime("midnight", $station['visitedAt']);
    if($new_day_start < $checkin_day_start) {
      //echo new header
      $output .= '<h4 class="ui sub header">' . get_day_name($station['visitedAt']) . '</h4>';
    }
    $checkin_day_start = $new_day_start;
    $output .=  '
    <div class="event">
      <div class="content">
        <div class="summary">
           ';
    if(!$guest) { $output .= date('H:i', $station['visitedAt']) . ' -'; }
    $output .= '<a class="stationLink" href="#" data-station="' . $station['station_id']. '">
             ' . $mode->stripStationName($station['commonName']) . '
           </a>
        </div>
      </div>
    </div>';
  }
  if(!$guest) {
    $output .= '
    </div>
    <div class="extra content">
    <a href="/history/completed" class="ui button">More History...</a>';
  }

  $output .= "</div>";

  return $output;
  }

function get_day_name($timestamp) {

    $date = date('d/m/Y', $timestamp);

    if($date == date('d/m/Y')) {
      $date = 'Today';
    }
    else if($date == date('d/m/Y', time() - (24 * 60 * 60))) {
      $date = 'Yesterday';
    }
    return $date;
}

function renderStationDetails($station_id, $parts = false) {
  global $mode;

  $venue = json_decode(json_decode($mode->getStationDetails($station_id),true), true)['response']['venue'];

  $venue_photos;
  foreach($venue['photos']['groups'] as $venue_photo_group) {
    if($venue_photo_group['type'] == "venue") {
      $venue_photos = $venue_photo_group;
      break;
    }
  }

  ob_start();
  ?>
  <div class="ui <?=$parts ? 'large' : 'medium'?> image">
    <a target="_blank" href="https://www.foursquare.com/v/<?=$venue['id']?>">
      <img id="stationImage" src="<?=$venue_photos['items'][0]['prefix']?>width960<?=$venue_photos['items'][0]['suffix']?>" class="ui rounded image">
    </a>
  </div>
  <?php

  $image = ob_get_contents();
  ob_end_clean();
  ob_start();

  ?>
  <div class="description">
      <h4 class="ui dividing header">Lines</h4>
      <?php
        //get the lines this station is on

        $lines = $mode->getLines(false, false, $station_id);

        foreach($lines as $line) { ?>
          <div class="ui <?=$line['line_name']?> horizontal label"><?=$line['display_name']?></div>
          <?php
        }

       ?>
    <h4 class="ui dividing header">Type</h4>

  <?php

  if(isset($venue['categories'][0])) {
  	echo '<image class="ui inline rounded tiny image black foursquare_icon" src="'.$venue['categories'][0]['icon']['prefix'].'88.png"/>' . $venue['categories']['0']['name'];
  } else {
  	echo '<image class="ui inline rounded tiny image black foursquare_icon" src="https://foursquare.com/img/categories/building/default_88.png"/>' . $venue['categories']['0']['name'];
  }

  //get zones for this station

  $zones = $mode->getZonesByStation($station_id);

  ?>
  <h4 class="ui dividing header">Zone</h4>
  <p>This station is in zone<?=count($zones)==1 ? "" : "s"?>

    <?php for($i = 0; $i < count($zones); $i++) {
            $zones_left = count($zones) - $i - 1;
            echo $zones[$i]['zone_name'];
            if($zones_left > 1) {
              echo ', ';
            }
            if($zones_left == 1) {
              echo ' and ';
            }
          }
    ?>.
    </p>

  <?php
  if(isset($_SESSION['user'])) { ?>
  <h4 class="ui dividing header">Visits</h4>
  <?php

    $stationData = $mode->getStationVisitData($station_id);

    if($stationData['checkins']) {
     ?>
    <p>You have visited <b><?=$stationData['checkins']?></b> time<?=$stationData['checkins']==1 ? "" : "s"?>.</br>
       You first visited on <b><?=date('d/m/y \a\t H:i', $stationData['firstCheckin'])?></b>.</br>
       You last visited on <b><?=date('d/m/y \a\t H:i', $stationData['lastCheckin'])?></b>.
     </p>
     <?php
   } else {
     ?>
     <p>You have not checked in at this station.
      </p>
      <p>
        <button class="ui labeled icon button completedButton" data-station="<?=$station_id?>">
          <i class="checkmark icon"></i>
          <span id="completedText">Mark as Completed</span>
        </button>
  </p>

  <?php } } ?>

  </div>
<?php
  $description = ob_get_contents();
  ob_end_clean();

  if($parts) {
    $output = Array('image' => $image,
                    'description' => $description);

    return $output;

  } else {
    return $image . $description;
  }

}


function echoError($message, $description = "Please try again.") {
  ?>
  <div class="ui very relaxed grid">
    <div class="row"></div>
    <div class="row">
      <div class="two wide column"></div>
      <div class="twelve wide center aligned column">
        <div class="ui negative visible message">
          <div class="header">
            <?=$message?>
          </div>
          <p><?=$description?>
          </p>
        </div>
      </div>
    </div>
  </div>
<?php
}


function renderTotalStationsVisitedCard($totalVisited) {
  ?>
  <div class="ui card">
    <div class="content">
      <div class="ui statistic">
        <div class="value">
          <?=$totalVisited?>
        </div>
        <div class="label">
          stations visited
        </div>
      </div>
    </div>
  </div>
  <?php
}

function renderProgressPercentageCard($progress_percent) {
  ?>
  <div class="ui card">
    <div class="content">
      <div class="ui statistic">
        <div class="value">
          <?=$progress_percent?>%
        </div>
        <div class="label">
          complete
        </div>
      </div>
    </div>
    <div id="stations_progress" class="ui bottom attached red progress">
      <div class="bar"></div>
    </div>
  </div>
  <?php
}

function renderStationsToGoCard($stations_to_go) {
  ?>
  <div class="ui card">
    <div class="content">
      <div class="ui statistic">
        <div class="value">
          <?=$stations_to_go?>
        </div>
        <div class="label">
          stations to go
        </div>
      </div>
    </div>
  </div>
  <?php
}

function renderUserProfileCard($user, $show_friends = true) {
  ?>
  <div class="ui card mobile hidden">
    <div>
      <img class="ui tiny centered rounded image" src="<?=$user['image']?>">
    </div>
    <div class="content">
      <a target="_blank" class="header" href="<?=$user['canonicalUrl']?>"><?=$user['first_name'] . ' ' . $user['last_name']?></a>
      <div class="meta">
        <span class="date">Joined in <?=date('Y', $user['joined4sq'])?></span>
      </div>
      <div class="description">
        <?=$user['bio']?>
      </div>
    </div>
    <?php
    if($show_friends) { ?>
    <div class="extra content">
      <a target="_blank" href="https://foursquare.com/activity">
        <i class="user icon"></i>
        <?=$user['friendsCount']?> Friends
      </a>
    </div>
    <?php } ?>
  </div>
  <?php
}

function renderRecentlyCompletedUICard($user_id = NULL) {
  global $db;

  if(!$user_id) {
    $user_id = $_SESSION['user']['user_id'];
  }
  ?>
  <div class="ui card">
    <div class="content">
      <div class="header">Recently Completed</div>
    </div>
    <div id="recentlyCompletedContent" class="content">

        <?php
        //check if we're still pulling this data
        $db->where('user_id', $user_id);
        $db->where('type', 2);
	 $db->orderBy('job_id', 'DESC');
        $job = $db->getOne('jobs');

        if($job['progress'] < 100) {
          //we're still pulling, display progress logic
          ?>
          <div class="container ui two column centered grid">
            <div class="four column centered row">
              <div class="column"></div>
              <div class="column"></div>
            </div>
            <div class="column centered-text row">
              <b>We're just gathering the data for here..</b>
            </div>
            <div class="column centered-text row">
              <div class="ui teal progress" id="progressBar" style="width:100%">
                <div class="bar">
                  <div class="progress"></div>
                </div>
              </div>
            </div>
            <div class="four column centered row">
              <div class="column"></div>
              <div class="column"></div>
            </div>
          </div>
          <script>

          function checkJob() {
            $.get( "/includes/checkjob?job_id=<?=$job['job_id']?>", function( data ) {
              $('#progressBar').progress('set percent', data);
              if(data == 100) {
                clearInterval(t);
                $.get('includes/getRecentlyCompleted?id=<?=$user_id?>', function(data) {
                  $('#recentlyCompletedContent').html(data);
                  $('#timeSelector').show();
                });
              }
            });
          }
          var t;
          $( document ).ready(function() {
            t=setInterval(checkJob,1000);
          });
          </script>
          <?php
        } else {
          echo renderRecentlyCompleted($user_id);
        } ?>
  </div>
</div>
<?php
}

function renderMapDropdown() {
  global $mode;
  ?>
  <div id="lineDropdown" class="ui floating pointing labeled icon dropdown button">
    <i class="filter icon"></i>
    <span class="text">Filter map by Line...</span>
    <div class="menu">
      <div class="ui icon search input">
        <i class="search icon"></i>
        <input type="text" placeholder="Search lines...">
      </div>
      <div class="divider"></div>
      <div class="header">
        <i class="tags icon"></i>
        Filter by Line
      </div>
      <div class="scrolling menu">
        <?php
          foreach($mode->getLines() as $line) {
        ?>
          <div class="item">
            <div class="ui <?=$line['line_name']?> empty circular label" data-value="<?=$line['line_id']?>"></div>
            <?=$line['display_name']?>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
  <?php
}

function rendergMapJavaScript($user_id) {
  global $mode;

  //we need an array of Lines for later on
  $lines = $mode->getLines();

  //make it easy to work with (indexed by line_id)
  $niceLines = Array();

  foreach($lines as $line) {
    $niceLines[$line['line_id']] = $line;
  }

  ?>
  <!-- Google Map Includes -->
  <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAPQe-54o4-F7Vj1m9iFtnW6cqvYhrTWec&callback=initMap">
      </script>
  <script>
  var customLabel = {
    restaurant: {
      label: 'R'
    },
    bar: {
      label: 'B'
    },
    unvisited: {
      icon: "https://maps.google.com/mapfiles/ms/icons/red-dot.png"
    },
    visited: {
      icon: "https://maps.google.com/mapfiles/ms/icons/green-dot.png"
    }
  };
  var map;
  var infoWindow;
  var gmarkers = [];
  var bounds;

  function initMap() {
    bounds = new google.maps.LatLngBounds();
    var uluru = {lat: 52.149, lng: -1.1495};
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(51.550353, -0.140503),
      mapTypeId: google.maps.MapTypeId.STREET
    });
    infoWindow = new google.maps.InfoWindow;
    // var marker = new google.maps.Marker({
    //   position: new google.maps.LatLng(52.149, -1.1495),
    //   map: map
    // });
    downloadUrl('/includes/map_markers?id=<?=$user_id?>', function(data) {
      var xml = data.responseXML;
      var markers = xml.documentElement.getElementsByTagName('marker');
      Array.prototype.forEach.call(markers, function(markerElem) {
        var linesData = <?=json_encode($niceLines)?>;
        var id = markerElem.getAttribute('id');
        var name = markerElem.getAttribute('name');
        var address = markerElem.getAttribute('address');
        var type = markerElem.getAttribute('type');
        var tfl_lines = markerElem.getAttribute('tfl_lines');
        var point = new google.maps.LatLng(
            parseFloat(markerElem.getAttribute('lat')),
            parseFloat(markerElem.getAttribute('lng')));

        var infowincontent = document.createElement('div');
        var strong = document.createElement('strong');
        strong.textContent = name
        infowincontent.appendChild(strong);
        infowincontent.appendChild(document.createElement('br'));

        //grab the lines from the received XML and render line badges

        linesArray = tfl_lines.split(',');

        for (let index = 0; index < linesArray.length; ++index) {
            lineData = linesData[linesArray[index]];
            var lineDiv = document.createElement('div');
            lineDiv.classList = "ui " + lineData['line_name'] + " horizontal label";
            lineDiv.append(lineData['display_name']);
            infowincontent.append(lineDiv);
        }

        var text = document.createElement('text');
        text.textContent = ''; //address
        infowincontent.appendChild(text);
        var icon = customLabel[type] || {};
        var marker = new google.maps.Marker({
          map: map,
          position: point,
          icon: icon.icon,
          tflLines: tfl_lines
        });

        bounds.extend(marker.position);

        marker.addListener('click', function() {
          infoWindow.setContent(infowincontent);
          infoWindow.open(map, marker);
        });

        gmarkers.push(marker);

      });
      map.fitBounds(bounds);
    });
  }

  function downloadUrl(url,callback) {
   var request = window.ActiveXObject ?
       new ActiveXObject('Microsoft.XMLHTTP') :
       new XMLHttpRequest;

   request.onreadystatechange = function() {
     if (request.readyState == 4) {
      //  request.onreadystatechange = doNothing;
       callback(request, request.status);
     }
   };

   request.open('GET', url, true);
   request.send(null);
  }

  $(document).ready(function() {

    $("#lineDropdown").dropdown({
      onChange: function(val) {
          var line_id = $(val).data('value');
          bounds = new google.maps.LatLngBounds();
          for (i = 0; i < gmarkers.length; i++) {
              marker = gmarkers[i];
              // If is same category or category not picked
              var line_array = marker.tflLines.split(',');
              if (line_array == line_id || line_array.indexOf(line_id.toString()) > -1) {
                  bounds.extend(marker.position);
                  marker.setVisible(true);
              }
              // Categories don't match
              else {
                // console.log('didn\'t match');
                  marker.setVisible(false);
              }
          }
          //re-fit map
          map.fitBounds(bounds);
        }
    });

  });
  </script>
<?php
}

function getUserYearsWithCheckins() {
  global $db;
  global $modeController;

// SELECT count(*), FROM_UNIXTIME(visitedAt,'%Y') as year FROM `user_checkins`
// INNER JOIN station_4sq_id on user_checkins.4sq_place_id = station_4sq_id.4sq_place_id
// INNER JOIN stations on station_4sq_id.station_id = stations.station_id
// where visitedAt > 0 and user_id = 32
// where mode_id = ?
// group by year

  $cols = Array("FROM_UNIXTIME(visitedAt,'%Y') as year");

  $db->where('user_id', $_SESSION['user']['user_id']);
  $db->where('visitedAt', 0, '>');
  $db->where('mode_id', $modeController->current_mode);
  $db->join('station_4sq_id', 'user_checkins.4sq_place_id = station_4sq_id.4sq_place_id', 'INNER');
  $db->join('stations', 'station_4sq_id.station_id = stations.station_id', 'INNER');
  $db->groupBy('year');
  $db->orderBy('year', 'DESC');
  $years = $db->get('user_checkins', null, $cols);

  return $years;
}

function getUsers($cols = NULL) {
  global $db;

  if (!$cols) {
    $cols = Array("user_id", "first_name", "last_name", "swarmUsername", "image");
  }

  $users = $db->get('users', null, $cols);

  return $users;
}

function getLeaderboardData() {
  global $db;
  global $mode;

  $users = getUsers();
  $count = 0;

  $totalStations = $mode->getTotalStations();

  foreach ($users as $user) {
    $totalVisited = $mode->getTotalVisitedStations($user['user_id']);

    $progress = $totalVisited / $totalStations;

    $users[$count]['visited'] = $totalVisited;
    $users[$count]['progress'] = floor(($progress) * 100);

    $count++;
  }

  return $users;
}

function renderHomepage() {
  global $mode;

  $totalVisited = $mode->getTotalVisitedStations();
  $totalStations = $mode->getTotalStations();
  $progress = $totalVisited / $totalStations;
  $progress_percent = floor(($progress) * 100);

  ?>
  <div class="ui three column stackable grid container">
    <div class="row">
      <div class="center aligned column">
        <?=renderTotalStationsVisitedCard($totalVisited)?>
      </div>
      <div class="center aligned column">
        <?=renderProgressPercentageCard($progress_percent)?>
      </div>
      <div class="center aligned column mobile hidden">
        <?=renderStationsToGoCard($totalStations - $totalVisited)?>
      </div>
    </div>
    <div class="row">
      <div class="center aligned column">
        <?=renderRecentlyCompletedUICard()?>
        </div>
        <div class="center aligned column">
          <?=renderUserProfileCard($_SESSION['user'])?>
          <div class="ui card">
            <div class="content">
              <div class="header">Top Stations</div>
            </div>
            <div class="content">
              <div class="ui small feed">
                <table class="ui very basic unstackable table">
                  <thead>
                    <tr>
                      <th>Station</th>
                      <th>Visits</th>
                    </tr>
                  </thead>
                  <tbody>

                  <?php
                  $stations = $mode->getTopStations();

                  foreach($stations as $station) {
                    ?>
                    <tr>
                      <td>
                        <a class="stationLink" href="#" data-station="<?=$station['station_id']?>">
                        <?=$mode->stripStationName($station['commonName'])?>
                        </a>
                      </td>
                      <td><?=$station['checkins']?></td>
                    </tr>
                    <?php
                  }
                   ?>
                  </tbody>
                </table>
            </div>
            <div class="extra content">
              <a href="/history/top" class="ui button">More History...</a>
            </div>
          </div>
        </div>
        </div>
        <div class="center aligned column">
            <div class="ui card">
              <div class="content">
                <div class="header">Recently Visited</div>
              </div>
              <div class="content">
                <div class="ui small feed">
                  <?php
                  $stations = $mode->getRecentlyVisitedStations();
                  $checkin_day_start = PHP_INT_MAX;

                  foreach($stations as $station) {

                    $new_day_start = strtotime("midnight", $station['visitedAt']);
                    if($new_day_start < $checkin_day_start) {
                      //echo new header
                      echo '<h4 class="ui sub header">' . get_day_name($station['visitedAt']) . '</h4>';
                    }
                    $checkin_day_start = $new_day_start;
                    ?>
                    <div class="event">
                      <div class="content">
                        <div class="summary">
                           <?=date('H:i', $station['visitedAt'])?> -
                           <a class="stationLink" href="#" data-station="<?=$station['station_id']?>">
                             <?=$mode->stripStationName($station['commonName'])?>
                           </a>
                        </div>
                      </div>
                    </div>
                    <?php
                  }
                   ?>

              </div>
              <div class="extra content">
                <a href="/history/visits" class="ui button">More History...</a>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>

  <script>
  $( document ).ready(function() {
    $('#stations_progress').progress({
      percent: <?=$progress_percent?>
    });
  });
  </script>
  <?php
}
?>
