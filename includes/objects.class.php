<?php

class station {

  var $naptanID;
  var $commonName;
  var $lat;
  var $lon;
  var $lastUpdated;
  var $zone;
  var $modeID = 1;

  public function is_valid() {
    return true;
  }

  public function getAsArray($no_zone) {
    $returnArray = Array('mode_id'     => $this->modeID,
                         'naptanID'    => $this->naptanID,
                         'commonName'  => $this->commonName,
                         'lat'         => $this->lat,
                         'lon'         => $this->lon,
                         'lastUpdated' => $this->lastUpdated
                 );

     if(!$no_zone) {
       $returnArray['zone'] = $this->zone;
     }

     return $returnArray;

  }

}
