<?php
$_SERVER['DOCUMENT_ROOT'] = '/var/www/html/tubemap';
//Autoload dependencies
require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
//Database
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/db_config.php');
//4sq
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/foursquare.php');
//Functions
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/functions.php');

$job_id = $argv[1];
$checkin_job_id = $argv[2];

$db->where('job_id', $job_id);
$user_id = $db->getValue('jobs', 'user_id');

$db->where('user_id', $user_id);
$user = $db->getOne('users');

$foursquare->SetAccessToken($user['4sq_token']);

echo 'Getting 250 checkins...';

$start_time = time();
$db->startTransaction();
getCheckins($start_time, true, $user_id); //get users last 250 checkins.
$db->commit();

echo "\n";
echo 'getting venuehistory';
updateProgress($job_id, 75);

//pull venue history
$request = $foursquare->GetPrivate("users/self/venuehistory");
$details = json_decode($request, true);

$db->startTransaction();
foreach($details['response']['venues']['items'] as $venue) {
	$venue = $venue['venue'];

	$place_id = get4sqPlaceID($venue['id'], $venue['name']);

	$data = Array('user_id' 		 => $user_id,
								'4sq_place_id' => $place_id,
							  'type'				 => 1
							 );

	$db->insert('user_checkins', $data);
}

updateProgress($job_id, 100);
echo "\n";
echo 'done';
$db->commit();

exec('bash -c "exec nohup setsid php /var/www/html/tubemap/includes/checkin_pull.php ' . $checkin_job_id . ' > /dev/null 2>&1 &"');

die();
