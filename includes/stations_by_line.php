<?php
session_start();
//Autoload dependencies
require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
//Database
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/db_config.php');
//Objects
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/objects.class.php');
//API
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/mode.class.php');
//ModeInit
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/modeInit.php');

// $line_id = $_GET['line_id'] ?? '';

if(isset($_GET['line_id']) && $_GET['line_id'] != '') {

  $line_id = $_GET['line_id'];

  //get stations by this line
  $cols = Array('stations.*', 'tfl_lines.*', 'count(user_checkins.checkin_id) as visited', '4sq_places.4sq_id');
  $db->where('lines_stations.line_id', $line_id);
  $db->join('tfl_lines', 'lines_stations.line_id = tfl_lines.line_id', 'INNER');
  $db->join('stations', 'lines_stations.station_id = stations.station_id', 'INNER');
  $db->join('station_4sq_id', 'stations.station_id = station_4sq_id.station_id', 'LEFT'); //might not be linked
  $db->join('4sq_places', 'station_4sq_id.4sq_place_id = 4sq_places.id', 'LEFT');
  $db->join('user_checkins', 'station_4sq_id.4sq_place_id = user_checkins.4sq_place_id AND user_checkins.user_id = ' . $_SESSION['user']['user_id'], 'LEFT'); //user might not have visited
  $db->orderBy('commonName', 'ASC');
  $db->groupBy('stations.station_id');
  $stations = $db->get('lines_stations', null, $cols);

  $line_describer = $mode->current_mode == 1 ? " Line" : "";

?>
<h3 id="lineHeader" class="ui header clear-margin-top"><?=$stations[0]['display_name'] . $line_describer?></h1>
  <table id="line_table" class="ui selectable celled unstackable sortable table">
    <thead>
      <tr>
        <th>Station</th>
        <th>Complete</th>
        <th>On</th>
        <th>sv</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach($stations as $station) {

        $station_string = $mode->stripStationName($station['commonName']);

        $unmapped_station = false;
        if(!isset($station['4sq_id']) || $station['4sq_id'] == NULL) {
          $unmapped_station = true;
        }
        ?>

        <tr>
          <td class="collapsing">
            <?php
            if($unmapped_station) { ?>
              <a class="unmapped-station" data-station="<?=$station['station_id']?>" href="#"><?=$station_string?></a>
              <?php } else { ?>
              <a class="stationLink" href="#" data-station="<?=$station['station_id']?>"><?=$station_string?></a>
              <?php }
           ?>

          </td>
          <td class="center aligned">
            <?php
              if($station['visited']) { ?>
                <i class="large green checkmark icon"></i>
                <?php } else { ?>
                <i class="large red remove icon"></i>
                <?php } ?>

          </td>
          <td></td>
          <td><?=$station['visited']?></td>
        </tr>

      <?php } ?>

    </tbody>
  </table>

  <?php
}
   ?>
