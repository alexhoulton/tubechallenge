<?php
session_start();
//Autoload dependencies
require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
//Database
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/db_config.php');
//4sq
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/foursquare.php');

if(isset($_GET['q']) && $_GET['q'] != '') {
  // Searching for venues nearby Montreal, Quebec
  $endpoint = "venues/search";

  $q = $_GET['q'];

  // Prepare parameters
  $params = array("query"=>$q, "near" => $q . ", United Kingdom");

  // Perform a request to a public resource
  $response = $foursquare->GetPublic($endpoint,$params);

  $venues = json_decode($response, true);

  $output_array = Array('success' => 'true', 'results' => Array());
  foreach($venues['response']['venues'] as $venue) {
    $venue_out = Array('name' => $venue['name'],
                       'value'   => $venue['id'],
                       'text' => $venue['name']);
    array_push($output_array['results'], $venue_out);
  }
  echo json_encode($output_array);
}
