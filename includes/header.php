<?php
ini_set('session.gc_maxlifetime', 604800);
ini_set('session.cookie_lifetime', 604800);
date_default_timezone_set('Europe/London');
session_start();
//Autoload dependencies
require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
//Database
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/db_config.php');
//Objects
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/objects.class.php');
//Auth
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/auth.class.php');
//Classes
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/mode.class.php');
//4sq
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/foursquare.php');
//Functions
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/functions.php');
//ModeInit
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/modeInit.php');

$auth = new auth($db);


$r = $_SERVER['REQUEST_URI'];

?>
<!doctype html>
<html>
  <head>
    <title>Tube Map Challenge</title>
    <link rel="stylesheet" type="text/css" href="/dist/semantic/dist/semantic.min.css">
    <link rel="stylesheet" type="text/css" href="/dist/css/custom.css">
    <link rel="stylesheet" type="text/css" href="/dist/css/mode_css">
    <link rel="stylesheet" type="text/css" href="/dist/css/dataTables.semanticui.min.css">

    <script src="/node_modules/jquery/dist/jquery.min.js"></script>
    <script src="/dist/semantic/dist/semantic.min.js"></script>
    <script src="/node_modules/jquery.cookie/jquery.cookie.js"></script>
    <script src="/dist/js/jquery.throttle-debounce.min.js"></script>
    <script src="/dist/js/tablesort.js"></script>
    <script src="/dist/js/dataTables/moment.min.js"></script>
    <script src="/dist/js/dataTables/jquery.dataTables.min.js"></script>
    <script src="/dist/js/dataTables/dataTables.semanticui.min.js"></script>
    <script src="/dist/js/dataTables/datetime-moment.js"></script>

    <meta content='width=device-width, initial-scale=1.0, user-scalable=yes' name='viewport' />

  </head>
  <body>
    <div class="ui inverted attached stackable menu">
      <div class="ui container">
        <a class="<?=$r == "/" ? "active " : ''?>item" href="/">
          <i class="home icon"></i>
        </a>
        <div class="ui simple dropdown item">
          <i class="<?=$modeController->getCurrentMode()['mode_glyph']?> icon"></i> <?=$modeController->getCurrentMode()['mode_name']?>
          <i class="dropdown icon"></i>
          <div class="menu">
            <?php
              foreach ($modeController->getModes() as $m) { ?>
                <a class="<?=$modeController->current_mode==$m['mode_id'] ? 'active' : ''?> mode item" href="#" data-pref="<?=$m['mode_id']?>"><i class="<?=$m["mode_glyph"]?> icon"></i> <?=$m["mode_name"]?></a>
            <?php
              }
             ?>
          </div>
        </div>

        <a class="<?=$r == "/line" ? "active " : ''?>item" href="/line">
          <i class="subway icon"></i> Stations by Line
        </a>
        <a class="<?=$r == "/zone" ? "active " : ''?>item" href="/zone">
          <i class="options icon"></i> Stations by Zone
        </a>
        <a class="<?=$r == "/map" ? "active " : ''?>item" href="/map">
          <i class="marker icon"></i> Map
        </a>
        <?php
        if($auth->checkPermissions(10)) { ?>
          <a class="<?=$r == "/leaderboard" ? "active " : ''?>item" href="/leaderboard">
            <i class="chart line icon"></i> Leaderboard
          </a>
        <?php } ?>

        <?php
        if(isset($_SESSION['user']['user_id'])) { ?>
          <div class="ui simple dropdown right item">
            <i class="user icon"></i> Welcome, <?=$_SESSION['user']['first_name']?>
            <i class="dropdown icon"></i>
            <div class="menu">
              <a class="item" href="/user/<?=$_SESSION['user']['swarmUsername'] ? $_SESSION['user']['swarmUsername'] : $_SESSION['user']['user_id']?>"><i class="user icon"></i> My Profile </a>
              <?php
              if($auth->checkPermissions(100)) { ?>
                <a id="impersonateLink" class="item" href="#"><i class="user icon"></i> Impersonate </a>
              <?php } ?>
              <?php
              if($auth->checkPermissions(100)) { ?>
                <a class="item" href="/db_update"><i class="settings icon"></i> DB Update </a>
              <?php } ?>
              <?php
              if(isset($_SESSION['user']['user_id'])) { ?>
		 <a class="item" href="/dataRefresh"><i class="refresh icon"></i> Refresh Data </a>
		 <a class="item" href="/logout"><i class="sign out icon"></i> Log Out </a>
              <?php } ?>
            </div>
          </div>
          <div class="item dropdownInMenu">
            <!-- <div class="ui input"><input type="text" placeholder="Search..."></div> -->
            <div id="searchDropdown" class="ui search selection dropdown">
              <input type="hidden" name="country">
              <i class="dropdown icon"></i>
              <div class="default text">Search stations...</div>
              <div class="menu">

              </div>
            </div>
          </div>
        <?php } else { ?>
          <a class="<?=strpos($r,"/login") !== false ? "active " : ''?>item" href="/login?page=/">
            <i class="sign in icon"></i> Login
          </a>
        <?php } ?>

      </div>
    </div>
