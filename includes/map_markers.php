<?php
session_start();
//Autoload dependencies
require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
//Database
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/db_config.php');
//Objects
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/objects.class.php');
//API
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/mode.class.php');
//lulwhat
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/domxml-php4-to-php5.php');
//ModeInit
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/modeInit.php');

if(isset($_GET['id']) && $_GET['id'] != '' && is_numeric($_GET['id'])) {
  $user_id = $_GET['id'];
} elseif(isset($_SESSION['user']['user_id'])) {
  $user_id = $_SESSION['user']['user_id'];
} else {
  die('invalid id');
}

$cols = Array('stations.*', 'count(user_checkins.checkin_id) as visited', '4sq_places.4sq_id',
              'group_concat(distinct lines_stations.line_id separator ",") as tfl_lines');
$db->where('mode_id', $modeController->current_mode);
$db->join('lines_stations', 'stations.station_id = lines_stations.station_id', 'INNER'); //stations with no line break the JS so remove them
$db->join('station_4sq_id', 'stations.station_id = station_4sq_id.station_id', 'LEFT'); //might not be linked
$db->join('4sq_places', 'station_4sq_id.4sq_place_id = 4sq_places.id', 'LEFT');
$db->join('user_checkins', 'station_4sq_id.4sq_place_id = user_checkins.4sq_place_id AND user_checkins.user_id = ' . $user_id, 'LEFT'); //user might not have visited
$db->groupBy('stations.station_id');
$stations = $db->get('stations', null, $cols);

if (isset($_GET['format']) && $_GET['format'] != '') {
  $format = $_GET['format'];
}

if ($format == "json") {

  $outArray = ["type" => "FeatureCollection",
               "features" => []];

  foreach($stations as $station) {

    $station['commonName'] = str_replace(' Underground Station', '', $station['commonName']);
    $station['commonName'] = str_replace(' Station', '', $station['commonName']);

    $child = [
      "type" => "Feature",
      "properties" => [
        "location" => $station['commonName'],
	"latitude" => $station['lat'],
	"description" => $station['commonName'],
	"thumb" => "",
	"credit" => "Transport for London",
	"objectid" => $station['station_id'],
	"creator" => "Transport for London",
	"longitude" => $station['lon'],
	"imagefile" => "",
	"date" => "",
	"discipline" => $station['visited'] ? 'Completed Station' : 'Unvisited Station',
        "line_ids" => array_map('intval', explode(",", $station['tfl_lines'])),
	"title" => $station['commonName'],
	"access" => "Public"
      ],
      "geometry" => [
        "type" => "Point",
	"coordinates" => [floatval($station['lon']),floatval($station['lat'])]
      ]
    ];

    array_push($outArray["features"], $child);

  }
  
  echo json_encode($outArray);

} else {

  // Start XML file, create parent node
  $doc = domxml_new_doc("1.0");
  $node = $doc->create_element("markers");
  $parnode = $doc->append_child($node);
  
  header("Content-type: text/xml");
  
  foreach($stations as $station) {
    // Add to XML document node
    $node = $doc->create_element("marker");
    $newnode = $parnode->append_child($node);
  
    $newnode->set_attribute("id", $station['station_id']);
    $newnode->set_attribute("name", $station['commonName']);
    $newnode->set_attribute("address", 'test');
    $newnode->set_attribute("lat", $station['lat']);
    $newnode->set_attribute("lng", $station['lon']);
    $newnode->set_attribute("type", $station['visited'] ? 'visited' : 'unvisited');
    $newnode->set_attribute("tfl_lines", $station['tfl_lines']);
  }
  
  echo $doc->dump_mem();
}
die();
?>
