<?php
ini_set('session.gc_maxlifetime', 604800);
ini_set('session.cookie_lifetime', 604800);
session_start();
//Autoload dependencies
require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
//Database
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/db_config.php');
//Auth
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/auth.class.php');
//API
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/mode.class.php');
//ModeInit
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/modeInit.php');

$auth = new auth($db);

if(!isset($_POST['user_id']) || $_POST['user_id'] == '') {
  die();
}

if(!$auth->checkPermissions(100)) {
  die();
}

$user_id = $_POST['user_id'];

$db->where('user_id', $user_id);
$data = $db->getOne('users');

if($data['user_id'] == $user_id) {
  $_SESSION['user'] = $data;
  $_SESSION['user']['user_id'] = $user_id;
}

die();
