<?php
ini_set('session.gc_maxlifetime', 604800);
ini_set('session.cookie_lifetime', 604800);
session_start();
//Autoload dependencies
require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
//Database
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/db_config.php');
//Objects
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/objects.class.php');
//API
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/mode.class.php');
//4sq
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/foursquare.php');
//Functions
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/functions.php');
//ModeInit
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/modeInit.php');

if(!isset($_GET['q']) || $_GET['q'] == '') {
  die('');
}

$search = $_GET['q'];

$db->where('mode_id', $mode->current_mode);
$db->where('commonName', '%' . $search . '%', 'LIKE');

$stations = $db->get('stations', 5);

$returnArray = Array('success' => true, 'results' => Array());

foreach($stations as $station) {
  $stationData = Array('name' => $mode->stripStationName($station['commonName']),
                   'value' => $station['station_id']);

  array_push($returnArray['results'], $stationData);

}

echo json_encode($returnArray);





?>
