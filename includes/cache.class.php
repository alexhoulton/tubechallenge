<?php

class cache {

  var $server_ip = '127.0.0.1';
  var $server_port = '11211';

  var $mem;

  public function __construct() {
    $this->mem = new Memcached();
    $this->mem->addServer($this->server_ip, $this->server_port);
  }

  public function checkCache($url) {

    $hash = md5($url);
    if($response = $this->mem->get($hash)) {
      return $response;
    } else {
      return false;
    }

  }

  public function addCache($url, $data) {

    $hash = md5($url);

    $this->mem->set($hash, $data, 3600);

    return true;
  }

}

?>
