<?php
session_start();
//Autoload dependencies
require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
//Database
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/db_config.php');
//Objects
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/objects.class.php');
//API
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/mode.class.php');
//lulwhat
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/domxml-php4-to-php5.php');
//ModeInit
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/modeInit.php');

if(isset($_GET['id']) && $_GET['id'] != '' && is_numeric($_GET['id'])) {
  $user_id = $_GET['id'];
} elseif(isset($_SESSION['user']['user_id'])) {
  $user_id = $_SESSION['user']['user_id'];
} else {
  die('invalid id');
}

$lines = $mode->getLines(true, $user_id);
echo json_encode($lines);
exit;
die();
?>
