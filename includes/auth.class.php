<?php

class auth {

  var $mem;
  var $db;

  public function __construct($db) {
    $this->db = $db;
    $this->mem = new cache();
  }

  public function getAuthLevel() {
    if(!isset($_SESSION['user']['user_id'])) {
      return 0;
    }
    
    $this->db->where('user_id', $_SESSION['user']['user_id']);
    $level = $this->db->getValue('user_roles', 'MAX(role_id)');

    if($level) {
      return $level;
    }
  }

  public function checkPermissions($level) {
    if($this->getAuthLevel() >= $level) {
      return true;
    }
    return false;
  }

}

?>
