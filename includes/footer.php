<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/basic_footer.php');
 ?>
<div id="stationModal" class="ui modal">
  <div id="stationName" class="header">Moorgate</div>
  <div id="stationLoader" class="ui segment">
    <div class="ui active inverted dimmer">
      <div class="ui text loader">Loading</div>
    </div>
    <p></p>
  </div>
  <div id="stationContent" class="scrolling image content">

  </div>
</div>
<?php
if($auth->checkPermissions(100)) { ?>
<div id="impersonateModal" class="ui small modal">
  <div class="header">
    Impersonate
  </div>
  <div class="content">
    <p>Please select a user to impersonate:</p>
    <div id="impersonateDropdown" class="ui fluid selection dropdown">
      <input type="hidden" name="user">
      <i class="dropdown icon"></i>
      <div class="default text">Select User</div>
      <div class="menu">
        <?php
          $db->where('user_id', $_SESSION['user']['user_id'], "!=");
          $users = $db->get('users');

          foreach($users as $user) { ?>
            <div class="item" data-value="<?=$user['user_id']?>">
              <img class="ui mini avatar image" src="<?=$user['image']?>">
              <?=$user['first_name'] . ' ' . $user['last_name']?>
            </div>
          <?php } ?>

      </div>
    </div>
  </div>
  <div class="actions">
    <div class="ui negative button">
      Go Back
    </div>
    <div class="ui positive right labeled icon button">
      Impersonate
      <i class="checkmark icon"></i>
    </div>
  </div>
</div>
<script>
$( document ).ready(function() {
  $('#impersonateLink').on('click', function(ev){
    ev.preventDefault();
    $('#impersonateModal').modal('setting', {
            transition  : 'fade up',
            onApprove : function() {
              $.post('/includes/impersonate', {user_id: $('#impersonateDropdown').dropdown('get value')}, function() {
                 window.location.replace("/");
              });
            }
    }).modal('show');
    $('#impersonateDropdown').dropdown();
  });
});
</script>
<?php } ?>
<script>
$( document ).ready(function() {
  $('.mode.item').on('click', function() {
    if($(this).hasClass('active')) { } else {

      $('.mode.item.active').removeClass('active');
      $(this).addClass('active');

      $.post('/includes/updateModePref', {mode_pref: $(this).data('pref')}, function() {
        location.reload();
      });
    }
  });
  $(document).on('click', '.stationLink', function(ev){
    ev.preventDefault();
    constructStationModal($(this).data('station'), $(this));
  });

  $('#timeSelector .ui.floating.dropdown.icon.button').dropdown();

  $(document).on('click', '.completedButton', function(ev){
    ev.preventDefault();
    var button = $(this);
    button.addClass('loading');
    button.addClass('disabled');
    $.post('/includes/completeStation', {station_id: $(this).data('station')}, function(data) {
      if(data==0) {
        button.removeClass('loading');
        button.addClass('negative');
        $('#completedText').text('That didn\'t work, please try again.');
        setTimeout(function(){
          $('#completedText').text('Mark as Completed');
          button.removeClass('disabled');
          button.removeClass('negative');
        }, 1000);
      } else {
        button.removeClass('loading');
        button.addClass('positive');
        $('#completedText').text('Completed!');
      }

    });
  });

  $('#searchDropdown').dropdown(
    {
      apiSettings: {
      // this url parses query server side and returns filtered results
      url: '/includes/stationSearch?q={query}',
      },
      onChange: function(value) {
        // send to station page
        window.location.replace("/station/" + value);
      }
    }
  );
});

function constructStationModal(station, object) {
  $('#stationLoader').show();
  $('#stationContent').hide();
  $('#stationModal').modal('setting', 'transition', 'fade up').modal('show');
  $('#stationName').text(object.text());
  $.get('/includes/getStationDetails?station_id=' + station, function(data) {
    $('#stationLoader').hide();
    $('#stationContent').html(data);
    $('#stationContent').show();

    $('#stationImage').bind('load', function(){ // image ready
        $('#stationModal').modal('refresh');
    });

  });

}

</script>
<script type="text/javascript">
var sc_project=11403358;
var sc_invisible=1;
var sc_security="23a12b6e";
</script>
<script type='text/javascript' src='https://secure.statcounter.com/counter/counter.js'></script>
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-37378135-2', 'auto');
  ga('send', 'pageview');

</script>
</body>
