<?php
//This file provides interaction with NR

class nr extends mode {
  var $api_root = "https://api.departureboard.io/api/v2.0/";

  private function send_api_call($endpoint) {
    //check cache first

    $url = $this->api_root . $endpoint;

    if($result = $this->mem->checkCache($url)) {
      return json_decode($result, true);
    } else {
      $context = stream_context_create([
          "http" => [
              "method"        => "GET",
              "ignore_errors" => true,
          ],
      ]);
      $result = file_get_contents($url, false, $context);

      $status_line = $http_response_header[0];
      preg_match('{HTTP\/\S*\s(\d{3})}', $status_line, $match);
      $status = $match[1];

      if ($status !== "200" && $status !== "500") {
          die("unexpected response status: {$status_line}\n" . $response);
      }
      $this->mem->addCache($url, $result);
      return json_decode($result, true);
    }

  }

  public function stripStationName($station) {
    $station = str_replace(' Railway Station', '', $station);
    $station = str_replace(' Station', '', $station);

    return $station;
  }

  public function getStations() {
    //returns an array of stations [crsCode, stationName]

    $url = "getStationBasicInfo/?station=";

    $stations = $this->send_api_call($url);

    return $stations;
  }

  public function getStationData($crs) {
    //returns an array of station data based on input CRS code

    $url = "getStationDetailsByCRS/" . $crs;

    $station = $this->send_api_call($url);

    return $station;
  }

  public function handleSpecialLine($action) {
    global $special_line;

    if($action == "populate") {
      // first ensure we removed old entries
      $this->handleSpecialLine("remove");
      // get stations that have no lines and add them to the special line
      $ids = $this->db->subQuery();
      $ids->get("lines_stations", null, "station_id");

      $this->db->where('station_id', $ids, 'NOT IN');
      $no_lines = $this->db->get('stations', null, 'stations.station_id');

      foreach ($no_lines as $station) {
        $data = Array('station_id' => $station['station_id'], 'line_id' => $special_line, 'lastUpdated' => time());

        $this->db->insert('lines_stations', $data);
      }
    } elseif($action == "remove") {
      $this->db->where('line_id', $special_line);
      $no_lines = $this->db->get('lines_stations', null, 'id');

      foreach ($no_lines as $station) {
        $this->db->where('id', $station['id']);
        $this->db->delete('lines_stations');
      }
    }
  }

  public function getStationTOCs($crs) {
    global $nr_api_key;
    // returns an array of TOCs at a station based on arrivals/departures

    $url = "getArrivalsAndDeparturesByCRS/" . $crs . "/?apiKey=" . $nr_api_key . "&serviceDetails=false&numServices=100";

    $services = $this->send_api_call($url);
    $tocs = [];

    if(!$services['trainServices']) {
      echo '</br>No services found for ' . $crs . '</br>';
    } else {
      foreach($services['trainServices'] as $service) {
        array_push($tocs, ["toc_code" => $service['operatorCode'], "toc_name" => $service['operator']]);
      }
    }
    return array_unique($tocs, SORT_REGULAR);
  }

  public function getZonesByStation($station_id) {
    $cols = Array('zones.*');
    $this->db->where('stations.station_id', $station_id);
    $this->db->join('stations_zones', 'zones.zone_id = stations_zones.zone_id', 'INNER');
    $this->db->join('stations', 'stations_zones.station_id = stations.station_id', 'INNER');
    $zones = $this->db->get('zones', null, $cols);

    return $zones;
  }

  private function dealWithZones($station_id, $zone) {

    //make sure the zone we get from TFL is created properly

    $zone_id = $this->getZoneID($zone, $station_id);

    if(mb_strlen($zone) == 3) {
      //we gotta create two zone entries
      //try split based on the middle character being a separator
      $string = str_split($zone);

      $zone_1_id = $this->getZoneID($string[0]);
      $zone_2_id = $this->getZoneID($string[2]);

      $this->insertZoneEntry($station_id, $zone_1_id);
      $this->insertZoneEntry($station_id, $zone_2_id);

    } else {

      $this->insertZoneEntry($station_id, $zone_id);

    }

  }

  private function getZoneID($zone, $station_id = NULL) {

    if(!$zone) {
      if($station_id != NULL) {
        //check if special or tram or other
        $this->db->where('station_id', $station_id);
        $station_name = $this->db->getValue('stations', 'commonName');
        if(strpos($station_name, 'Tram') !== false) {
          $zone = 'T';
        } else {
          $zone = 'S';
        }
      }
    }

    //get zone_id
    $this->db->where('zone_name', $zone);
    $zone_id = $this->db->getValue('zones', 'zone_id');

    if(!$zone_id) {
      $data = Array('zone_name' => $zone, 'display_name' => $zone);

      $zone_id = $this->db->insert('zones', $data);
    }

    return $zone_id;
  }

  private function insertZoneEntry($station_id, $zone_id) {

    $data = Array('station_id' => $station_id, 'zone_id' => $zone_id);

    return $this->db->insert('stations_zones', $data);
  }

  public function populate_database($debug = false) {
    $this->db->startTransaction();

    $this->handleSpecialLine("remove");

    $stations = $this->getStations();

    foreach($stations as $s) {
      // go get detailed station data from API

      $pulled_station = $this->getStationData($s['crsCode']);

      $station = new station();

      $station->modeID = $this->current_mode;
      $station->naptanID = $pulled_station['crsCode'];
      $station->commonName = $pulled_station['name'];
      $station->lat = $pulled_station['latitude'];
      $station->lon = $pulled_station['longitude'];
      $station->lastUpdated = time();

      //check if the station exists
      $this->db->where('naptanId', $station->naptanID);
      $station_id = $this->db->getValue('stations', 'station_id');

      if($station_id) {
        $this->db->where('station_id', $station_id);
        $this->db->update ('stations', $station->getAsArray(1));
      } else {
        $station_id = $this->db->insert ('stations', $station->getAsArray(1));
        if($debug) {
          echo $this->db->getLastQuery() . '<br>';
        }
      }

      //deal with zones
      if($pulled_station['fares']['travelcardZones'] && count($pulled_station['fares']['travelcardZones']) > 0) {
        $matches = [];
        preg_match('/Zone ([0-9]+[\/\+]*[0-9]*)/', $pulled_station['fares']['travelcardZones'][0], $matches);
        $station->zone = $matches[1];
        $this->dealWithZones($station_id, $station->zone);
      }

      //go get the next few trains at this station to figure out TOCs that call here..
      $tocs = $this->getStationTOCs($station->naptanID);

      foreach($tocs as $toc) {
         //check if the line entry exists for this TOC
         $this->db->where('line_name', $toc['toc_code']);
         $this->db->where('mode_id', $this->current_mode);
         $line_id = $this->db->getValue('tfl_lines', 'line_id');

         if(!$line_id) {
           //create the TOC
           $data = Array('mode_id' => $this->current_mode,
                         'line_name' => $toc['toc_code'],
                         'display_name' => $toc['toc_name']);

           $line_id = $this->db->insert('tfl_lines', $data);
           if($debug) {
             echo $this->db->getLastQuery() . '<br>';
           }
         }

         //check if the line<->station mapping exists
         $this->db->where('station_id', $station_id);
         $this->db->where('line_id', $line_id);
         $line_index = $this->db->getValue('lines_stations', 'id');

         $data = Array('station_id' => $station_id,
                       'line_id' => $line_id,
                       'lastUpdated' => time()
                      );

         if($line_index) {
           $this->db->where('id', $line_index);
           $this->db->update('lines_stations', $data);
         } else {

           $line_index = $this->db->insert('lines_stations', $data);
           if($debug) {
             echo $this->db->getLastQuery() . '<br>';
           }
         }
      }

    }
    $this->handleSpecialLine("populate");
    $this->db->commit();
  }

}



 ?>
