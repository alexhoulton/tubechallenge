<?php
ini_set('session.gc_maxlifetime', 604800);
ini_set('session.cookie_lifetime', 604800);
session_start();
//Autoload dependencies
require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
//Database
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/db_config.php');
//Auth
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/auth.class.php');
//API
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/mode.class.php');
//ModeInit
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/modeInit.php');

$auth = new auth($db);

if(!isset($_POST['station_id']) || $_POST['station_id'] == '') {
  die('0');
}

$station_id = $_POST['station_id'];

$user_id = $_SESSION['user']['user_id'];

//get 4sq_place_id

$db->where('station_id', $station_id);
$place_id = $db->getValue('station_4sq_id', '4sq_place_id');

if(!$place_id) {
  die('0');
}

$data = Array('user_id'       => $user_id,
              'visitedAt'     => time(),
              '4sq_place_id'  => $place_id,
              'type'          => 3);

$checkin_row = $db->insert('user_checkins', $data);

if(!$checkin_row) {
  die('0');
}

die('1');
