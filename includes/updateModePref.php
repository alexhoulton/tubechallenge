<?php
session_start();
//Autoload dependencies
require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
//Database
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/db_config.php');

if(isset($_POST['mode_pref']) && $_POST['mode_pref'] != '' && is_numeric($_POST['mode_pref'])) {
  //update db
  $modePref = $_POST['mode_pref'];
  $data = Array('current_mode' => $modePref);

  $db->where('user_id', $_SESSION['user']['user_id']);
  if($db->update('users', $data)) {
    $_SESSION['user']['current_mode'] = $modePref;
  }

}
