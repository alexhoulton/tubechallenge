<?php
$_SERVER['DOCUMENT_ROOT'] = '/var/www/html/tubemap';
//Autoload dependencies
require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
//Database
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/db_config.php');
//4sq
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/foursquare.php');
//Functions
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/functions.php');

//quick hack because why not (sorry)

$db2 = new MysqliDb ($creds);

$job_id = $argv[1];
// $job_id = $_GET['job_id'];

$db->where('job_id', $job_id);
$user_id = $db->getValue('jobs', 'user_id');

$db->where('user_id', $user_id);
$user = $db->getOne('users');

$foursquare->SetAccessToken($user['4sq_token']);

echo 'Getting checkins...';

$start_time = time();

$db->startTransaction();
$totalCheckins = 0;
$checkinsPulled = 0;
getCheckins($start_time, false, $user_id, $job_id); //get users checkins.
$db->commit();

updateProgress($job_id, 100);
echo "\n";
echo 'done';
$db->commit();

die();
