<?php
//Header
require_once('includes/header.php');
//Authentication
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/authentication.php');
//4sq
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/foursquare.php');

if($auth->checkPermissions(100)) {
  if($modeController->current_mode==1) {

  }

  if($modeController->current_mode==2) {
    //try and populate stations automatically based on a name format of "xxx Railway Station (CRS)"
    //find unmapped stations
    $ids = $db->subQuery();
    $ids->get("station_4sq_id", null, "station_id");

    $db->where('station_id', $ids, 'NOT IN');
    $db->where('mode_id', $modeController->current_mode);
    $db->orderBy("RAND ()");
    $unmapped = $db->get('stations', 150, 'stations.*');

    foreach ($unmapped as $station) {
      $station_string = $station['commonName'] . " Railway Station (" . $station['naptanId'] . ")";
      echo '</br>' . $station_string . ": " . $station['station_id'];
      // Prepare parameters
      $station['noBrackets'] = preg_replace('/\ \([A-Za-z\ ]*\)/', '', $station['commonName']);
      $station_string2 = $station['noBrackets'] . " Railway Station (" . $station['naptanId'] . ")";
      $params = array("query"=>$station['noBrackets'], "near" => $station['naptanId'] . ", United Kingdom");

      // Perform a request to a public resource
      $endpoint = "venues/search";
      $response = $foursquare->GetPublic($endpoint,$params);

      $venues = json_decode($response, true);

      $output_array = Array('success' => 'true', 'results' => Array());
      foreach($venues['response']['venues'] as $venue) {
        if ($venue['name'] == $station_string || $venue['name'] == $station_string2) {
          echo ': found match with ' . $venue['name'];
          $station_id = $station['station_id'];
          $place_id = $venue['id'];
          $place_name = $venue['name'];

          //check if the 4sq place already exists

          $db->where('4sq_id', $place_id);
          $foursquare_places_id = $db->getValue('4sq_places', 'id');

          if(!$foursquare_places_id) {
            //create it

            $data = Array('4sq_id' => $place_id,
                          '4sq_name' => $place_name);

            $foursquare_places_id = $db->insert('4sq_places', $data);

          }

          //now insert the mapping

          $data = Array('station_id' => $station_id,
                        '4sq_place_id' => $foursquare_places_id);

          $db->insert('station_4sq_id', $data);
        }
      }
    }
  }
}

?>

<?php
//Footer
require_once('includes/footer.php');
 ?>
