<?php
//Header
require_once('includes/header.php');
//Authentication
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/authentication.php');

if(!isset($_GET['id']) || $_GET['id'] == '') {
  require_once('includes/footer.php');
  die('Invalid Station');
}

$station_id = $_GET['id'];

$db->where('station_id', $station_id);
$station = $db->getOne('stations');

if($station['station_id'] != $station_id) {
  require_once('includes/footer.php');
  die('Invalid Station');
}

$station_data = renderStationDetails($station_id, true);

?>
<div class="ui one column stackable grid container clear-margin-top">
  <div class="column">
    <h1 class="ui dividing header"><?=$mode->stripStationName($station['commonName'])?></h1>
  </div>
</div>
<div class="ui two column stackable grid container clear-margin-top">
  <div class="seven wide center aligned column">
    <?=$station_data['image']?>
  </div>
  <div class="nine wide column">
    <?=$station_data['description']?>
  </div>
</div>

<?php
//Footer
require_once('includes/footer.php');
 ?>
