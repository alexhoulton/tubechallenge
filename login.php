<?php
//Header
require_once('includes/header.php');

?>
<div class="ui two column centered grid clear-margin-top">
  <div class="column centered-text">
    <h2 class="ui header"></div>
  </div>
  <div class="four column centered row">
    <div class="column"></div>
    <div class="column"></div>
  </div>
</div>
<div class="ui stackable very relaxed center aligned grid container">
  <div class="row">
    <div class="twelve wide column">
      <h1 class="ui homepage header">Tube Map Challenge</h1>
      <p>Digitally check off all the stations as you visit them! Integrates with <a target="_blank" href="https://www.swarmapp.com">Swarm</a> and TFL data to make magic happen.</p>
    </div>
  </div>
</div>


<div class="ui two column centered grid">
  <div class="column centered-text">
    <a href="<?=$foursquare->AuthenticationLink($redirect_uri)?>?page=<?=htmlspecialchars($_GET['page'])?>" class="ui orange button">
      <img class="ui avatar image" src="/dist/images/swarm.png">
      <span>Connect with Swarm</span>
    </a>
  </div>
  <div class="four column centered row">
    <div class="column"></div>
    <div class="column"></div>
  </div>
</div>
<div class="stackable very relaxed ui grid container">
    <div class="row">
      <div class="seven wide column">
        <h2 class="ui homepage header">Detailed Progress Data</h2>
        <p>You can check your progress by Line, or by Zone.</p>
        <p>The number of stations completed, in additon to the total number are displayed, along with your percentage progress.</p>
        <p>New <a target="_blank" href="https://www.swarmapp.com">Swarm</a> checkins are automatically checked for new stations, and marked as complete automatically.</p>
        <p></p>
      </div>
      <div class="nine wide column">
        <div class="ui existing segment"><img class="ui rounded centered image" src="/images/line.png"></div>
      </div>
    </div>
    <div class="ui divider"></div>
    <div class="row">
      <div class="seven wide column">
        <h2 class="ui homepage header">Statistics</h2>
        <p>Statistics provide an at a glance view of progress. </p>
        <p>They include all lines and stations, and give an overall progress indication</p>
        <p></p>
      </div>
      <div class="nine wide column">
        <div class="ui existing segment"><img class="ui centered rounded image" src="/images/completion.png"></div>
      </div>
    </div>
    <div class="ui divider"></div>
    <div class="row">
      <div class="seven wide column">
        <h2 class="ui homepage header">Map View</h2>
        <p>Map view allows you to take a geographical look at your progress, showing areas most completed. </p>
        <p>You can filter by line, to show which stations remain.</p>
        <p></p>
      </div>
      <div class="nine wide column">
        <div class="ui existing segment"><img class="ui centered rounded image" src="/images/map.png"></div>
      </div>
    </div>
  </div>
<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/basic_footer.php');
 ?>
