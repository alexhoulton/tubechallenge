<?php
session_start();
//Autoload dependencies
require_once($_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php');
//Database
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/db_config.php');
//4sq
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/foursquare.php');
//Functions
require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/functions.php');

$db->where('user_id', $_SESSION['user']['user_id']);
$db->where('type', 2);
$db->orderBy('job_id', 'DESC');
$job = $db->getOne('jobs');

if($job['progress'] < 100) {
  header("Location:/");
  die();
}

$data = Array('type' => 2, 'user_id' => $_SESSION['user']['user_id']);

$checkin_job_id = $db->insert('jobs', $data);

exec('bash -c "exec nohup setsid php /var/www/html/tubemap/includes/checkin_pull.php ' . $checkin_job_id . ' > /dev/null 2>&1 &"');

header("Location:/");
die();
